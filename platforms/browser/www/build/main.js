webpackJsonp([19],{

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RestProvider = (function () {
    function RestProvider(http) {
        this.http = http;
        this.apiUrl = 'http://app.kitvision.org/api';
        console.log('Hello RestProvider Provider');
    }
    RestProvider.prototype.login = function (data) {
        var _this = this;
        this.username = data.username;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/login/tech', JSON.stringify(data))
                .subscribe(function (res) {
                // login successful if there's a jwt token in the response
                var token = res['token'];
                if (token) {
                    // set token property
                    _this.token = token;
                    var permissions = [];
                    for (var _i = 0, _a = res['permissions']; _i < _a.length; _i++) {
                        var data_1 = _a[_i];
                        permissions.push(data_1.permission);
                    }
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ "username": _this.username, "token": _this.token, "permissions": permissions.join(',') }));
                    localStorage.setItem('userInfo', JSON.stringify({ "avatar": res['user'].avatar, "phone": res['user'].phone, "name": res['user'].name, 'rating': res['user'].rating }));
                    _this.subscribeGPS = new __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */]().watchPosition({ maximumAge: 30000, timeout: 10000, enableHighAccuracy: true }).subscribe(function (position) {
                        console.log(position.coords.latitude);
                        _this.setLoc(_this.token, position.coords.latitude, position.coords.longitude).then(function (result) {
                            //do nothing
                        });
                    });
                    // return true to indicate successful login
                    resolve(res);
                }
                else {
                    // return false to indicate failed login
                    resolve(res);
                }
                //console.log(res['token']);
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.logout = function () {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
        localStorage.removeItem('userInfo');
        this.subscribeGPS.unsubscribe();
    };
    RestProvider.prototype.resetpwd = function (email) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('email', email);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/reset', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.setLoc = function (token, lat, long) {
        var _this = this;
        //console.log(token);
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('lat', lat)
            .set('long', long)
            .set('api_token', token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/location/set', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                //console.log(res);
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getFiles = function (token) {
        var _this = this;
        //console.log(token);
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/files/troubleshoot', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                //console.log(res);
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getHistory = function (token) {
        var _this = this;
        //console.log(token);
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/all', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                console.log(res);
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getTickets = function (token) {
        var _this = this;
        //console.log(token);
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/assigned', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                console.log(res);
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getRemote = function () {
        var _this = this;
        //console.log(token);
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/remote/all', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                console.log(res);
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.changePassword = function (data) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('password', data.password)
            .set('api_token', this.token)
            .set('con_password', data.con_password);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/user/changepwd', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.changePhone = function (phone) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', this.token)
            .set('phone', phone);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/user/changePhone', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getAllChats = function () {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/chat/all', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                console.log(res);
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getChatStatus = function () {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/chat/status/get', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.setChatStatus = function (status) {
        var _this = this;
        if (this.token == null)
            return;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('client', '0')
            .set('status', status)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/chat/status/set', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getMessages = function (thread_id) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('thread_id', thread_id)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/chat/get', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.sendMessage = function (message, client_id, thread_id) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('message', message)
            .set('thread_id', thread_id)
            .set('client_id', client_id)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/chat/sendMessage', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getTicketDetails = function (ticket_id, lat, long) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('ticket_id', ticket_id)
            .set('latitude', lat)
            .set('longitude', long)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/getTicket', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.declineTicket = function (ticket_id) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('ticket_id', ticket_id)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/decline', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.acceptTicket = function (ticket_id, lat, long) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('ticket_id', ticket_id)
            .set('latitude', lat)
            .set('longitude', long)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/accept', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.startTicket = function (ticket_id, lat, long) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('ticket_id', ticket_id)
            .set('latitude', lat)
            .set('longitude', long)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/start', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.stopTicket = function (ticket_id, message, status_id) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('ticket_id', ticket_id)
            .set('message', message)
            .set('status', status_id)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/stop', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getScreenHistory = function (screen_id) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('screen_id', screen_id)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/screen/all', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getAllStatuses = function () {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/status/all', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.savePhoto = function (ticket_id, img_data) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('ticket_id', ticket_id)
            .set('image_data', img_data)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/savePhoto', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.saveSign = function (ticket_id, img_data) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('ticket_id', ticket_id)
            .set('image_data', img_data)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/saveSignature', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.saveAnswer = function (ticket_id, questions_data) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('ticket_id', ticket_id)
            .set('question_data', JSON.stringify(questions_data))
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/answers', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getQuestions = function () {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/ticket/survey', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.setChatToken = function (token_id) {
        var _this = this;
        var body = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            .set('chat_token', token_id)
            .set('api_token', this.token);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + '/chat/setToken', body.toString(), {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], RestProvider);
    return RestProvider;
}());

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ChatDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChatDetailsPage = (function () {
    function ChatDetailsPage(navCtrl, navParams, restProvider, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
    }
    ChatDetailsPage.prototype.ionViewWillEnter = function () {
        this.username = this.navParams.get('username');
        this.loadchat();
    };
    ChatDetailsPage.prototype.loadchat = function () {
        var _this = this;
        this.restProvider.getMessages(this.navParams.get('thread_id')).then(function (result) {
            /* Array.prototype.sort.call(result, function (a, b) {
                 if (a.chat_id > b.chat_id) {
                     return 1;
                 }
                 if (a.chat_id < b.chat_id) {
                     return -1;
                 }
                 return 0;
             });*/
            _this.messages = result;
        }, function (err) {
            console.log(err);
        });
    };
    ChatDetailsPage.prototype.sendmsg = function () {
        var _this = this;
        this.restProvider.sendMessage(this.message_data, this.navParams.get('user_id'), this.navParams.get('thread_id'))
            .then(function (result) {
            if (result['error']) {
                _this.presentToast("Something goes wrong");
            }
            else {
                _this.presentToast(result['msg']);
                _this.message_data = "";
                _this.loadchat();
            }
        }, function (err) {
            console.log(err);
        });
    };
    ChatDetailsPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.present();
    };
    ChatDetailsPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.restProvider.getMessages(this.navParams.get('thread_id')).then(function (result) {
            /* Array.prototype.sort.call(result, function (a, b) {
                 if (a.chat_id > b.chat_id) {
                     return 1;
                 }
                 if (a.chat_id < b.chat_id) {
                     return -1;
                 }
                 return 0;
             });*/
            _this.messages = result;
            refresher.complete();
        }, function (err) {
            console.log(err);
            refresher.complete();
        });
    };
    ChatDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-chat-details',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/chat-details/chat-details.html"*/'<!--\n  Generated template for the ChatDetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{username}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n            pullingIcon="arrow-dropdown"\n            pullingText="Pull to refresh"\n            refreshingSpinner="circles"\n            refreshingText="Loading...">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <ion-list class="list">\n    <ion-item class="item" *ngFor="let message of messages">\n      <ion-avatar item-start>\n        <img src="{{(message.user.avatar == null || message.user.avatar == \'\' )?\'assets/avatar.png\': message.user.avatar}}">\n      </ion-avatar>\n      <h2>{{message.user.name}}</h2>\n      <p class="sp0" >{{message.message}} </p>\n    </ion-item>\n\n  </ion-list>\n\n  <div class="send_message_wrapper">\n    <textarea class="description" placeholder="" [(ngModel)]="message_data"></textarea>\n\n    <div  class="btnSend">\n      <button (click)="sendmsg()"  class="sendBtn">SEND</button>\n    </div>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/chat-details/chat-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], ChatDetailsPage);
    return ChatDetailsPage;
}());

//# sourceMappingURL=chat-details.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chat_details_chat_details__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChatPage = (function () {
    function ChatPage(navCtrl, navParams, restProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
    }
    ChatPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.restProvider.getAllChats().then(function (result) {
            _this.chats = result;
        }, function (err) {
            console.log(err);
        });
    };
    ChatPage.prototype.chatclick = function (user_id, username, thread_id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__chat_details_chat_details__["a" /* ChatDetailsPage */], { 'user_id': user_id, 'username': username, 'thread_id': thread_id });
    };
    ChatPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.restProvider.getAllChats().then(function (result) {
            _this.chats = result;
            refresher.complete();
        }, function (err) {
            console.log(err);
            refresher.complete();
        });
    };
    ChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-chat',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/chat/chat.html"*/'<ion-content align="center" padding>\n  <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n  <hr class="logohr">\n  <h2 class="imp"><ion-label align="center" TextDecorations="Underline">CHAT</ion-label>\n  </h2><hr class="logohr">\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n            pullingIcon="arrow-dropdown"\n            pullingText="Pull to refresh"\n            refreshingSpinner="circles"\n            refreshingText="Loading...">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <ion-list class="list" style="overflow: initial;max-height: 100%;">\n    <ion-item class="item" *ngFor="let chat of chats" (click)="chatclick(chat.client_id,chat.user.name, chat.thread_id)">\n      <ion-avatar item-start>\n        <div class="online-status" *ngIf="chat.status!=null"></div>\n        <img src="{{(chat.user.avatar == null || chat.user.avatar == \'\' )?\'assets/avatar.png\': chat.user.avatar}}">\n      </ion-avatar>\n      <h2>{{chat.user.name}}</h2>\n      <p class="sp0" >{{chat.chats.message}} </p>\n    </ion-item>\n\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/chat/chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */]])
    ], ChatPage);
    return ChatPage;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClientsignPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_signaturepad_signature_pad__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_signaturepad_signature_pad___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_signaturepad_signature_pad__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ClientsignPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ClientsignPage = (function () {
    function ClientsignPage(toastCtrl, navCtrl, navParams, loadingCtrl, restProvider, alertCtrl) {
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.restProvider = restProvider;
        this.alertCtrl = alertCtrl;
        this.signaturePadOptions = {
            'minWidth': 2,
            'canvasWidth': 340,
            'canvasHeight': 200
        };
    }
    ClientsignPage.prototype.submit = function () {
        var _this = this;
        this.signatureImage = this.signaturePad.toDataURL();
        if (this.signatureImage.length == 0) {
            var alert_1 = this.alertCtrl.create({
                title: 'Signuature',
                subTitle: 'Please sign before moving next',
                buttons: ['Dismiss']
            });
            alert_1.present();
            return;
        }
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.restProvider.saveSign(this.navParams.get('ticket_id'), this.signatureImage).then(function (result) {
            loading.dismiss();
            _this.presentToast(result['msg']);
        });
    };
    ClientsignPage.prototype.presentToast = function (msg) {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            _this.navCtrl.popToRoot();
        });
        toast.present();
    };
    ClientsignPage.prototype.drawClear = function () {
        this.signaturePad.clear();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2_angular2_signaturepad_signature_pad__["SignaturePad"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_angular2_signaturepad_signature_pad__["SignaturePad"])
    ], ClientsignPage.prototype, "signaturePad", void 0);
    ClientsignPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-clientsign',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/clientsign/clientsign.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>TICKET CLIENT SIGN</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content align="center" padding>\n    <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n    <hr class="logohr">\n    <h2><ion-label align="center" TextDecorations="Underline">CLIENT SIGNATURE</ion-label>\n    </h2><hr class="logohr">\n    <br><br>\n    <ion-img class="signLogo" align="center" src="assets/imgs/sign.png"></ion-img>\n\n    <signature-pad [options]="signaturePadOptions"  id="signatureCanvas"></signature-pad>\n          \n          <br>\n    <button round class="clientSignBtn" (click)="drawClear()" >Clear Signature</button>\n    <button round class="clientSignBtn" (click)="submit()">SUBMIT</button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/clientsign/clientsign.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], ClientsignPage);
    return ClientsignPage;
}());

//# sourceMappingURL=clientsign.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chat_chat__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__history_history__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profile_profile__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__remotearea_remotearea__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__trb_shoot_trb_shoot__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_rest_rest__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_geolocation__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__tickets_tickets__ = __webpack_require__(121);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DashboardPage = (function () {
    function DashboardPage(navCtrl, navParams, restProv, push, alertCtrl, app, geolocation) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProv = restProv;
        this.push = push;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.geolocation = geolocation;
        this.chat = false;
        this.file_perm = false;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__chat_chat__["a" /* ChatPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_3__history_history__["a" /* HistoryPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_5__remotearea_remotearea__["a" /* RemoteareaPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_4__profile_profile__["a" /* ProfilePage */];
        this.tab5Root = __WEBPACK_IMPORTED_MODULE_6__trb_shoot_trb_shoot__["a" /* TrbShootPage */];
        this.tab6Root = __WEBPACK_IMPORTED_MODULE_10__tickets_tickets__["a" /* TicketsPage */];
        this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
        if (this.currentUser.permissions.indexOf('chat') !== -1) {
            this.chat = true;
            this.restProv.setChatStatus(1);
        }
        if (this.currentUser.permissions.indexOf('trouble_shoot') !== -1) {
            this.file_perm = true;
        }
        // to check if we have permission
        this.initPush();
        //load location to the server
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.restProv.setLoc(_this.currentUser.token, resp.coords.latitude, resp.coords.longitude).then(function (result) {
                console.log(result['msg']);
            });
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
        var subscription = this.geolocation.watchPosition()
            .subscribe(function (position) {
            if (position.coords.longitude != undefined) {
                _this.restProv.setLoc(_this.currentUser.token, position.coords.latitude, position.coords.longitude).then(function (result) {
                    console.log(result['msg']);
                });
                console.log(position.coords.longitude + ' ' + position.coords.latitude);
            }
        });
        //for chat status
        this.task = setInterval(function () {
            _this.restProv.setChatStatus(1);
        }, 30000);
    }
    DashboardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DashboardPage');
    };
    DashboardPage.prototype.initPush = function () {
        var _this = this;
        var options = {
            android: {
                senderID: '800934935444',
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push',
            }
        };
        var pushObject = this.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            if (notification.message.indexOf('chat') != -1)
                _this.presentChatAlert(notification.message);
            else if (notification.message.indexOf('ticket') != -1)
                _this.presentTicketAlert(notification.message);
            else
                _this.presentAlert(notification.message);
        });
        pushObject.on('registration').subscribe(function (registration) {
            _this.restProv.setChatToken(registration.registrationId).then(function (result) {
                console.log(result['msg']);
            });
            console.log('Device registered', registration.registrationId);
        });
        pushObject.on('error').subscribe(function (error) { return console.error('Error with Push plugin', error); });
    };
    DashboardPage.prototype.presentAlert = function (msg) {
        var alert = this.alertCtrl.create({
            title: 'Message',
            subTitle: msg,
            buttons: ['Dismiss']
        });
        alert.present();
    };
    DashboardPage.prototype.presentChatAlert = function (msg) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'New Chat',
            subTitle: msg,
            buttons: [{
                    text: 'OK',
                    role: 'confirm',
                    handler: function () {
                        //this.navCtrl.push(ChatPage);
                        _this.app.getRootNav().getActiveChildNav().select(4);
                    }
                }]
        });
        alert.present();
    };
    DashboardPage.prototype.presentTicketAlert = function (msg) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Ticket',
            subTitle: msg,
            buttons: [{
                    text: 'OK',
                    role: 'confirm',
                    handler: function () {
                        _this.app.getRootNav().getActiveChildNav().select(3);
                    }
                }]
        });
        alert.present();
    };
    DashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-dashboard',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/dashboard/dashboard.html"*/'  \n    <ion-tabs  color="tabs" class="tabs" selectedIndex="0" >\n      <ion-tab [root]="tab1Root"text-wrap *ngIf="chat" tabTitle="CHAT" tabIcon="chat"></ion-tab>\n      <ion-tab [root]="tab2Root" tabTitle="HISTORY" tabIcon="history"></ion-tab>\n      <ion-tab [root]="tab3Root" tabTitle="REMOTE" tabIcon="remote"></ion-tab>\n      <ion-tab [root]="tab4Root" tabTitle="PROFILE" tabIcon="profile"></ion-tab>\n      <ion-tab [root]="tab5Root" tabTitle="FILES" *ngIf="file_perm" tabIcon="troubleshoot"></ion-tab>\n      <ion-tab [root]="tab6Root" tabTitle="TICKETS"  tabIcon="history"></ion-tab>\n      </ion-tabs>'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/dashboard/dashboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_7__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__["a" /* Push */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_geolocation__["a" /* Geolocation */]])
    ], DashboardPage);
    return DashboardPage;
}());

//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ticketupdate_ticketupdate__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__techrequest_techrequest__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__techbooked_techbooked__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__jobdone_jobdone__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__takephoto_takephoto__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HistoryPage = (function () {
    function HistoryPage(navCtrl, navParams, toastCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.restProvider = restProvider;
        this.userInfo = JSON.parse(localStorage.getItem('userInfo'));
        this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.getAllHistory(this.currentUserData.token);
    }
    HistoryPage.prototype.ionViewWillEnter = function () {
        this.userInfo = JSON.parse(localStorage.getItem('userInfo'));
        this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.getAllHistory(this.currentUserData.token);
    };
    HistoryPage.prototype.getAllHistory = function (token) {
        var _this = this;
        this.restProvider.getHistory(token)
            .then(function (result) {
            /*console.log("here i am");
            console.log(result);*/
            if (result['error']) {
                _this.presentToast("Something goes wrong");
            }
            else {
                _this.history = result;
            }
        }, function (err) {
            console.log(err);
        });
    };
    HistoryPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.present();
    };
    HistoryPage.prototype.ticketUpdate = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__ticketupdate_ticketupdate__["a" /* TicketupdatePage */]);
    };
    HistoryPage.prototype.ticketclick = function (ticket_id, approved, screen_id) {
        console.log(ticket_id);
        if (approved == 0)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__techrequest_techrequest__["a" /* TechrequestPage */], { 'ticket_id': ticket_id });
        else if (approved == 1)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__techbooked_techbooked__["a" /* TechbookedPage */], { 'ticket_id': ticket_id, 'screen_id': screen_id });
        else if (approved == 2)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__jobdone_jobdone__["a" /* JobdonePage */], { 'ticket_id': ticket_id, 'screen_id': screen_id });
        else if (approved == 3)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__takephoto_takephoto__["a" /* TakephotoPage */], { 'ticket_id': ticket_id, 'screen_id': screen_id });
    };
    HistoryPage.prototype.doRefresh = function (refresher) {
        this.getAllHistory(this.currentUserData.token);
        refresher.complete();
    };
    HistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-history',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/history/history.html"*/'<ion-content align="center" padding>\n  <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n  <hr class="logohr">\n  <h2 class="techbook"><ion-label align="center" TextDecorations="Underline">TICKETS HISTORY</ion-label></h2>\n <hr class="logohr">\n <br>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content\n                pullingIcon="arrow-dropdown"\n                pullingText="Pull to refresh"\n                refreshingSpinner="circles"\n                refreshingText="Loading...">\n        </ion-refresher-content>\n    </ion-refresher>\n  <!--<div class="PicRating">\n    <div class="prfpic" align="left">\n      <div  style="padding-top:4px; padding-left:-2px;" class="cardlabel2P2">\n        \n        <ion-img style="margin-left:13px; background-color:green;" class="onlineImg"></ion-img>\n        <ion-img style="margin-left:-8px;" class="profilerating" src="{{(this.userInfo.avatar==\'\')?\'assets/avatar.png\':this.userInfo.avatar}}"></ion-img>\n        \n        <ion-label class="profiletexts" >{{this.userInfo.name}}</ion-label>\n        <ion-label class="profiletexts">{{this.userInfo.phone}}</ion-label>\n     </div>  \n    </div>\n                    <div class="prating" align="right">\n                      <div class="prating">\n                          <ion-icon name="star" *ngIf="this.userInfo.rating > 0"></ion-icon>\n                          <ion-icon name="star" *ngIf="this.userInfo.rating > 1"></ion-icon>\n                          <ion-icon name="star" *ngIf="this.userInfo.rating > 2"></ion-icon>\n                          <ion-icon name="star" *ngIf="this.userInfo.rating > 3"></ion-icon>\n                          <ion-icon name="star" *ngIf="this.userInfo.rating > 4"></ion-icon>\n                          <ion-icon name="star-outline" *ngIf="this.userInfo.rating < 1"></ion-icon>\n                          <ion-icon name="star-outline" *ngIf="this.userInfo.rating < 2"></ion-icon>\n                          <ion-icon name="star-outline" *ngIf="this.userInfo.rating < 3"></ion-icon>\n                          <ion-icon name="star-outline" *ngIf="this.userInfo.rating < 4"></ion-icon>\n                          <ion-icon name="star-outline" *ngIf="this.userInfo.rating < 5"></ion-icon>\n                      </div> \n    </div>\n  </div>\n  <br>-->\n<ion-card class="cardContainer" *ngFor="let ticket of history" (click)="ticketclick(ticket.ticket_id,ticket.approved,ticket.screen_id)">\n          <ion-card-header class="cardheader-h">\n                  <div class="client-info">\n                      <ion-avatar>\n                          <img src="{{(ticket.picture==\'\')?\'assets/avatar.png\':ticket.picture}}">\n                      </ion-avatar>\n                      <p>{{ticket.name}} - {{ticket.phone}}</p>\n                      <p>{{ ticket.created_at | date: \'dd-M-yyyy\'}}</p>\n                  </div>\n                  <div class="cardlabel">\n                       <ion-label class="ticket"><font color="red">ID# {{ticket.ticket_id}}</font></ion-label>\n                       <ion-label class="date"  ion-text [style.color]="ticket.color">{{ticket.status}}</ion-label>\n                  </div>\n\n                  <div class="cardlabel2">\n                     <div class="cardlabel2P1">\n                        <ion-label class="labelfirst">{{ticket.screen_name}}</ion-label>\n                        <ion-label style="margin:0px;">\n                            <div class="rating">\n                                <ion-icon name="star" *ngIf="ticket.rating > 0"></ion-icon>\n                                <ion-icon name="star" *ngIf="ticket.rating > 1"></ion-icon>\n                                <ion-icon name="star" *ngIf="ticket.rating > 2"></ion-icon>\n                                <ion-icon name="star" *ngIf="ticket.rating > 3"></ion-icon>\n                                <ion-icon name="star" *ngIf="ticket.rating > 4"></ion-icon>\n                                <ion-icon name="star-outline" *ngIf="ticket.rating < 1"></ion-icon>\n                                <ion-icon name="star-outline" *ngIf="ticket.rating < 2"></ion-icon>\n                                <ion-icon name="star-outline" *ngIf="ticket.rating < 3"></ion-icon>\n                                <ion-icon name="star-outline" *ngIf="ticket.rating < 4"></ion-icon>\n                                <ion-icon name="star-outline" *ngIf="ticket.rating < 5"></ion-icon>\n                            </div>\n                        </ion-label>\n                      </div>\n\n                      </div>\n                     <!-- <div class="labelfirsts-h" >\n                      <ion-label item-start class="labelfirsts">{{ticket.org_name}}</ion-label>\n                    </div>-->\n\n                   <!-- <div style="padding-top:-5px;" class="personDetails-h">\n                        <ion-label class="ratingLabel">Rating</ion-label>\n\n                        </div>-->\n\n                  </ion-card-header>\n          <ion-card-content class="cardContent">\n              <div style="margin-top:-18px;" class="cardContent">\n                  <div class="message">\n                    <p>Main Message</p>\n                  </div>\n                  <div class="messagedetails">\n                    <p>{{ticket.problem_desc}}</p>\n                  </div>\n              </div>\n         </ion-card-content>\n        </ion-card>\n\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/history/history.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */]])
    ], HistoryPage);
    return HistoryPage;
}());

//# sourceMappingURL=history.js.map

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReviewtechPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__clientsign_clientsign__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ReviewtechPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReviewtechPage = (function () {
    function ReviewtechPage(navCtrl, navParams, restProvider, toastCtrl, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.restProvider.getQuestions().then(function (result) {
            _this.questions = result;
        });
    }
    ReviewtechPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReviewtechPage');
    };
    ReviewtechPage.prototype.nexton = function () {
    };
    ReviewtechPage.prototype.next = function () {
        var _this = this;
        var flag = false;
        for (var index = 0; index < this.questions.length; index++) {
            if (this.questions[index].selected_answer === undefined) {
                flag = true;
            }
        }
        if (flag) {
            var alert_1 = this.alertCtrl.create({
                title: 'Complete Feedback',
                subTitle: 'Please select all answers before moving next',
                buttons: ['Dismiss']
            });
            alert_1.present();
            return;
        }
        this.restProvider.saveAnswer(this.navParams.get('ticket_id'), this.questions).then(function (result) {
            _this.presentToast(result['msg']);
        });
    };
    ReviewtechPage.prototype.presentToast = function (msg) {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__clientsign_clientsign__["a" /* ClientsignPage */], { 'ticket_id': _this.navParams.get('ticket_id') });
        });
        toast.present();
    };
    ReviewtechPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-reviewtech',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/reviewtech/reviewtech.html"*/'<!--\n  Generated template for the ReviewtechPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>TICKET REVIEW</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content align="center" padding>\n    <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n    <hr class="logohr">\n    <h2><ion-label align="center" TextDecorations="Underline">QUESTIONS FOR CLIENT</ion-label>\n    </h2><hr class="logohr">\n    <br>\n    <ion-card *ngFor="let question of questions;let i = index;">\n    <ion-list radio-group [(ngModel)]="question.selected_answer"  >\n\n        <ion-list-header>\n         <ion-label class="question-title">{{question.question}}</ion-label>\n        </ion-list-header>\n      \n        <ion-item style="padding-top:-10px; padding-left:30px;border-bottom:0px">\n          <ion-label style="padding-left:-5px;">{{question.ch_1}}</ion-label>\n          <ion-radio item-start value="{{question.ch_1}}"  ></ion-radio>\n        </ion-item>\n      \n        <ion-item style="padding-left:30px;border-bottom:0px" *ngIf="question.ch_2!=null">\n          <ion-label>{{question.ch_2}}</ion-label>\n          <ion-radio item-start value="{{question.ch_2}}" ></ion-radio>\n        </ion-item>\n      \n        <ion-item style="padding-left:30px;border-bottom:0px" *ngIf="question.ch_3!=null">\n          <ion-label>{{question.ch_3}}</ion-label>\n          <ion-radio item-start value="{{question.ch_3}}"  ></ion-radio>\n        </ion-item>\n\n        <ion-item style="padding-left:30px;border-bottom:0px" *ngIf="question.ch_4!=null">\n          <ion-label>{{question.ch_4}}</ion-label>\n          <ion-radio item-start value="{{question.ch_4}}"  ></ion-radio>\n        </ion-item>\n\n        <ion-item style="padding-left:30px;border-bottom:0px" *ngIf="question.ch_5!=null">\n            <ion-label>{{question.ch_5}}</ion-label>\n            <ion-radio item-start value="{{question.ch_5}}"  ></ion-radio>\n        </ion-item>\n      </ion-list>\n    </ion-card>\n    <br>\n\n        <br><br><br>\n        <button round class="nexttBtn" (click)="next()" >NEXT</button>\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/reviewtech/reviewtech.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], ReviewtechPage);
    return ReviewtechPage;
}());

//# sourceMappingURL=reviewtech.js.map

/***/ }),

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StartrepairPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__jobdone_jobdone__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__previoustickets_previoustickets__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the StartrepairPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var StartrepairPage = (function () {
    function StartrepairPage(navCtrl, navParams, restProvider, geolocation, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.geolocation = geolocation;
        this.toastCtrl = toastCtrl;
    }
    StartrepairPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad StartrepairPage');
    };
    StartrepairPage.prototype.next = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__jobdone_jobdone__["a" /* JobdonePage */]);
    };
    StartrepairPage.prototype.startRepair = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.restProvider.startTicket(_this.navParams.get('ticket_id'), resp.coords.latitude, resp.coords.longitude).then(function (result) {
                _this.presentToast(result['msg']);
            });
        }).catch(function (error) {
            //this.presentToast('Location not found. Please try again');
        });
    };
    StartrepairPage.prototype.presentToast = function (msg) {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            _this.navCtrl.popToRoot();
        });
        toast.present();
    };
    StartrepairPage.prototype.prevHistory = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__previoustickets_previoustickets__["a" /* PreviousticketsPage */], { 'screen_id': this.navParams.get('screen_id') });
    };
    StartrepairPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-startrepair',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/startrepair/startrepair.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>START REPAIR</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content align="center" padding>\n  <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n  <hr class="logohr">\n  <h2 class="imp"><ion-label align="center" TextDecorations="Underline">TECHNICIAN REQUEST</ion-label>\n  </h2><hr class="logohr">\n  <br><br>\n  <div>\n    <ion-img style="padding-top:6px;" class="repairImage" align="center" src="assets/imgs/startrepair.png"></ion-img>\n  </div>\n\n\n  <br><br>\n  <button  round class="ticketUpdate" align="center" (click) = "startRepair()">START THE REPAIR</button>\n  <br><br><br><br><br>\n  <button  round class="ticketUpdate" align="center" (click) = "prevHistory()">Previous History</button>\n</ion-content>\n\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/startrepair/startrepair.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], StartrepairPage);
    return StartrepairPage;
}());

//# sourceMappingURL=startrepair.js.map

/***/ }),

/***/ 117:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = (function () {
    function ProfilePage(alertCtrl, navCtrl, navParams, restProvider, app, toastCtrl) {
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.app = app;
        this.toastCtrl = toastCtrl;
        this.password_data = { con_password: '', password: '' };
        this.phone_data = { phone: '' };
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.logout = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Are you sure?',
            message: 'Do you want to Logout.',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Logout',
                    handler: function () {
                        _this.restProvider.setChatStatus(0);
                        _this.restProvider.logout();
                        _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
                    }
                }
            ]
        });
        confirm.present();
    };
    ProfilePage.prototype.changepwd = function () {
        var _this = this;
        if (this.password_data.password != this.password_data.con_password) {
            var confirm_1 = this.alertCtrl.create({
                title: 'Password Mismatch',
                message: 'Password didnot match with confirm password',
                buttons: [
                    {
                        text: 'OK',
                        handler: function () {
                            //do nothing
                        }
                    }
                ]
            });
            confirm_1.present();
            return;
        }
        if (this.password_data.password == '' || this.password_data.con_password == '') {
            var confirm_2 = this.alertCtrl.create({
                title: 'Enter Password',
                message: 'Please enter password',
                buttons: [
                    {
                        text: 'OK',
                        handler: function () {
                            //do nothing
                        }
                    }
                ]
            });
            confirm_2.present();
            return;
        }
        this.restProvider.changePassword(this.password_data).then(function (result) {
            if (result['error']) {
                _this.presentToast(result['error']);
            }
            else {
                var confirm_3 = _this.alertCtrl.create({
                    title: 'Password Changed',
                    message: result['msg'],
                    buttons: [
                        {
                            text: 'OK',
                            handler: function () {
                                _this.restProvider.logout();
                                _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
                            }
                        }
                    ]
                });
                confirm_3.present();
            }
        }, function (err) {
            console.log(err);
        });
    };
    ProfilePage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.present();
    };
    ProfilePage.prototype.changePhone = function () {
        var _this = this;
        if (this.phone_data.phone == '') {
            var confirm_4 = this.alertCtrl.create({
                title: 'Enter Phone',
                message: 'Please enter phone number',
                buttons: [
                    {
                        text: 'OK',
                        handler: function () {
                            //do nothing
                        }
                    }
                ]
            });
            confirm_4.present();
            return;
        }
        this.restProvider.changePhone(this.phone_data.phone).then(function (result) {
            if (result['error']) {
                _this.presentToast(result['error']);
            }
            else {
                _this.presentToast(result['msg']);
                _this.phone_data.phone = '';
            }
        }, function (err) {
            console.log(err);
        });
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/profile/profile.html"*/'<ion-content align="center" padding>\n        <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n        <hr class="logohr">\n        <h2><ion-label align="center" TextDecorations="Underline">PROFILE</ion-label>\n        </h2><hr class="logohr">\n\n<br/><div>\n       <!-- <ion-img align="center" class="profilePic" src="assets/avatar.png"></ion-img>\n        <ion-icon class="pen" ios="ios-create" md="md-create"></ion-icon>\n       \n<br/>\n      <hr class="logohr"> -->\n      <ion-label>CHANGE PASSWORD</ion-label>\n      <ion-input class="input" type="password" placeholder="Enter Password" [(ngModel)]="password_data.password"></ion-input>\n      <ion-input class="input" type="password" placeholder="Confirm Password" [(ngModel)]="password_data.con_password"></ion-input>\n      <button  round class="buttonLogout" (click)="changepwd()">RESET PASSWORD</button>\n</div><br/>\n<hr class="logohr">\n      <ion-label>MODIFY PHONENUMBER</ion-label>\n      <ion-input type="number" class="input" placeholder="Modify PhoneNumber" [(ngModel)]="phone_data.phone"></ion-input>\n      <button  round class="buttonLogout" (click)="changePhone()">UPDATE NUMBER</button>\n      <br><br>\n      <hr class="logohr">\n      <ion-label>LOGOUT</ion-label>\n      <button  round class="buttonLogout" align="center" (click)="logout()">LOGOUT</button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 118:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetpasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ResetpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ResetpasswordPage = (function () {
    function ResetpasswordPage(alertCtrl, navCtrl, navParams, restProvider, toastCtrl) {
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.credential = { email: '' };
    }
    ResetpasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResetpasswordPage');
    };
    ResetpasswordPage.prototype.resetEmail = function () {
        var _this = this;
        this.restProvider.resetpwd(this.credential.email).then(function (result) {
            console.log(result);
            var alert = _this.alertCtrl.create({
                title: 'DONE',
                message: result['msg'],
                buttons: [
                    {
                        text: 'OK',
                        role: 'OK',
                        handler: function () {
                            console.log('Ok clicked');
                        }
                    }
                ]
            });
            alert.present();
        }, function (err) {
            console.log(err);
        });
    };
    ResetpasswordPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000
        });
        toast.present();
    };
    ResetpasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-resetpassword',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/resetpassword/resetpassword.html"*/'<!--\n  Generated template for the ResetpasswordPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>resetpassword</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content align="center" padding>\n\n  <br><br><br><br><br><br><br><br><br>\n      <ion-img  align="center" class="Logo" src="assets/kitvision.png"></ion-img>\n      <br><br><br><br>\n    \n        <ion-input class="input" placeholder="Enter Your Email" [(ngModel)]="credential.email"></ion-input>\n        <br/>\n<button  round class="btnSubmit" align="center" (click) = "resetEmail()">SUBMIT</button>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/resetpassword/resetpassword.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], ResetpasswordPage);
    return ResetpasswordPage;
}());

//# sourceMappingURL=resetpassword.js.map

/***/ }),

/***/ 119:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RemoteareaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the RemoteareaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RemoteareaPage = (function () {
    function RemoteareaPage(navCtrl, navParams, restProvider, toastCtrl, alertCtrl, geolocation) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.geolocation = geolocation;
        this.userInfo = JSON.parse(localStorage.getItem('userInfo'));
        this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.getAllHistory();
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.latitude = resp.coords.latitude;
            _this.longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    }
    RemoteareaPage.prototype.ionViewWillEnter = function () {
        this.getAllHistory();
    };
    RemoteareaPage.prototype.getAllHistory = function () {
        var _this = this;
        this.restProvider.getRemote()
            .then(function (result) {
            /*console.log("here i am");
            console.log(result);*/
            if (result['error']) {
                _this.presentToast("Something goes wrong");
            }
            else {
                _this.history = result;
            }
        }, function (err) {
            console.log(err);
        });
    };
    RemoteareaPage.prototype.doRefresh = function (refresher) {
        this.getAllHistory();
        refresher.complete();
    };
    RemoteareaPage.prototype.reject = function (ticket_id) {
        var _this = this;
        {
            var alert_1 = this.alertCtrl.create({
                title: 'Confirm',
                message: 'Do you want to Reject Request?',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function () {
                            console.log('cancel clicked');
                        }
                    },
                    {
                        text: 'OK',
                        role: 'OK',
                        handler: function () {
                            _this.restProvider.declineTicket(ticket_id).then(function (result) {
                                _this.presentToast(result['msg']);
                            }, function (err) {
                                console.log(err);
                            });
                        }
                    }
                ]
            });
            alert_1.present();
        }
    };
    RemoteareaPage.prototype.presentToast = function (msg) {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            _this.getAllHistory();
        });
        toast.present();
    };
    RemoteareaPage.prototype.accept = function (ticket_id) {
        var _this = this;
        {
            var alert_2 = this.alertCtrl.create({
                title: 'Confirm',
                message: 'Do you want to Accept Request?',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function () {
                            console.log('cancel clicked');
                        }
                    },
                    {
                        text: 'OK',
                        role: 'OK',
                        handler: function () {
                            _this.restProvider.acceptTicket(ticket_id, _this.latitude, _this.longitude).then(function (result) {
                                _this.presentToast(result['msg']);
                            }, function (err) {
                                console.log(err);
                            });
                        }
                    }
                ]
            });
            alert_2.present();
        }
    };
    RemoteareaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-remotearea',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/remotearea/remotearea.html"*/'<ion-content align="center" padding>\n    <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n    <hr class="logohr">\n    <h2 class="techbook"><ion-label align="center" TextDecorations="Underline">REMOTE AREA</ion-label>\n    </h2><hr class="logohr">\n\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content\n                pullingIcon="arrow-dropdown"\n                pullingText="Pull to refresh"\n                refreshingSpinner="circles"\n                refreshingText="Loading...">\n        </ion-refresher-content>\n    </ion-refresher>\n\n    <br/>\n\n    <!--<ion-label *ngIf=\'(history | async)?.length === 0\'>\n        No Pending Remote Requests\n    </ion-label>-->\n\n    <ion-card class="cardContainer" *ngFor="let ticket of history" >\n        <ion-card-header class="cardheader-h">\n            <div class="cardlabel">\n                <ion-label class="ticket"><font color="red">Ticket ID#{{ticket.ticket_id}}</font></ion-label>\n                <ion-label class="date">{{ ticket.created_at | date: \'dd-M-yyyy\'}}</ion-label>\n            </div>\n            <!--<hr class="hrLine">-->\n            <div class="cardlabel2">\n                <div class="cardlabel2P1">\n                    <ion-label class="labelfirst">{{ticket.screen_name}}</ion-label>\n                    <ion-label style="margin-left:24px;" class="status">{{ticket.status}}</ion-label>\n                </div>\n            </div>\n            <div class="cardlabel2">\n                <div class="cardlabel2P1">\n                    <ion-label class="labelfirst">Scheduled Date</ion-label>\n                    <ion-label style="margin-left:24px;" class="status">{{ticket.schedule_date | date: \'dd-M-yyyy\'}}</ion-label>\n                </div>\n            </div>\n            <div class="labelfirsts-h" >\n                <ion-label item-start class="labelfirsts">{{ticket.org_name}}</ion-label>\n            </div>\n            <!--<hr class="hrLine">-->\n        </ion-card-header>\n        <ion-card-content class="cardContent">\n            <div style="margin-top:-18px;" class="cardContent">\n                <div class="message">\n                    <p>Main Message</p>\n                </div>\n                <div class="messagedetails">\n                    <p>{{ticket.problem_desc}}</p>\n                </div>\n            </div>\n\n            <div class="actionCircle" style="padding-top:5px;">\n                <div style=" width: 56%;\n      height: 90px;" style="padding-left:20px;"  class="decline circle-decline" align="left">\n                    <a (click)="reject(ticket.ticket_id)"><ion-thumbnail class="circleImage" > <img src="assets/imgs/reject.png"></ion-thumbnail></a>\n                </div>\n                <div style="width: 56%;\n      height: 90px;" style="padding-right:20px;" class="accept circle-accept" align="right">\n                    <a (click)="accept(ticket.ticket_id)"><ion-thumbnail class="circleImage"><img src="assets/imgs/accept.png"></ion-thumbnail></a>\n                </div>\n            </div>\n\n        </ion-card-content>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/remotearea/remotearea.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */]])
    ], RemoteareaPage);
    return RemoteareaPage;
}());

//# sourceMappingURL=remotearea.js.map

/***/ }),

/***/ 120:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrbShootPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TrbShootPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TrbShootPage = (function () {
    function TrbShootPage(navCtrl, navParams, restProvider, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.searchTerm = '';
        this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.getAllFiles(this.currentUserData.token);
    }
    TrbShootPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TrbShootPage');
    };
    TrbShootPage.prototype.getAllFiles = function (token) {
        var _this = this;
        this.restProvider.getFiles(token)
            .then(function (result) {
            if (result['error']) {
                _this.presentToast("Something goes wrong");
            }
            else {
                _this.files = result;
                _this.allFiles = result;
            }
        }, function (err) {
            console.log(err);
        });
    };
    TrbShootPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000
        });
        toast.present();
    };
    TrbShootPage.prototype.filterItems = function (searchTerm) {
        //this.getAllFiles(this.currentUserData.token);
        return this.files.filter(function (item) {
            return item.file_name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    };
    TrbShootPage.prototype.setFilteredItems = function () {
        this.files = this.allFiles;
        //this.getAllFiles(this.currentUserData.token);
        this.files = this.filterItems(this.searchTerm);
    };
    TrbShootPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-trb-shoot',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/trb-shoot/trb-shoot.html"*/'\n<ion-content align="center" padding>\n    <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n    <hr class="logohr">\n    <h2 class="imp"><ion-label align="center" TextDecorations="Underline">TROUBLESHOOT FILES</ion-label>\n    </h2><hr class="logohr">\n\n\n\n\n  <ion-searchbar class="searchbar"\n  [(ngModel)]="searchTerm" (ionInput)="setFilteredItems()" \n  placeholder="Search Our Library">\n</ion-searchbar>\n<br/>\n<ion-list class="list">\n    <ion-item *ngFor="let file of files">\n      <ion-thumbnail item-start class="docImage">\n          <img class="docImage" src="assets/pdf.png" alt="{{file.file_name}}" />\n      </ion-thumbnail>\n      <p class="file-desc"> {{ file.file_name }}</p>\n      <a href="{{ file.path }}"><ion-thumbnail item-end class="downImage"><img src="assets/download.png"></ion-thumbnail></a>\n    </ion-item>\n</ion-list>\n</ion-content>\n    '/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/trb-shoot/trb-shoot.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], TrbShootPage);
    return TrbShootPage;
}());

//# sourceMappingURL=trb-shoot.js.map

/***/ }),

/***/ 121:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TicketsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ticketupdate_ticketupdate__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__jobdone_jobdone__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__techbooked_techbooked__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__takephoto_takephoto__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__techrequest_techrequest__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the TicketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TicketsPage = (function () {
    function TicketsPage(navCtrl, navParams, toastCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.restProvider = restProvider;
        this.userInfo = JSON.parse(localStorage.getItem('userInfo'));
        this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.getTickets(this.currentUserData.token);
    }
    TicketsPage.prototype.ionViewWillEnter = function () {
        this.userInfo = JSON.parse(localStorage.getItem('userInfo'));
        this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.getTickets(this.currentUserData.token);
    };
    TicketsPage.prototype.getTickets = function (token) {
        var _this = this;
        this.restProvider.getTickets(token)
            .then(function (result) {
            /*console.log("here i am");
            console.log(result);*/
            if (result['error']) {
                _this.presentToast("Something goes wrong");
            }
            else {
                _this.history = result;
            }
        }, function (err) {
            console.log(err);
        });
    };
    TicketsPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.present();
    };
    TicketsPage.prototype.ticketUpdate = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__ticketupdate_ticketupdate__["a" /* TicketupdatePage */]);
    };
    TicketsPage.prototype.ticketclick = function (ticket_id, approved, screen_id) {
        console.log(ticket_id);
        if (approved == 0)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__techrequest_techrequest__["a" /* TechrequestPage */], { 'ticket_id': ticket_id });
        else if (approved == 1)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__techbooked_techbooked__["a" /* TechbookedPage */], { 'ticket_id': ticket_id, 'screen_id': screen_id });
        else if (approved == 2)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__jobdone_jobdone__["a" /* JobdonePage */], { 'ticket_id': ticket_id, 'screen_id': screen_id });
        else if (approved == 3)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__takephoto_takephoto__["a" /* TakephotoPage */], { 'ticket_id': ticket_id, 'screen_id': screen_id });
    };
    TicketsPage.prototype.doRefresh = function (refresher) {
        this.getTickets(this.currentUserData.token);
        refresher.complete();
    };
    TicketsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-tickets',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/tickets/tickets.html"*/'<ion-content align="center" padding>\n  <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n  <hr class="logohr">\n  <h2 class="techbook"><ion-label align="center" TextDecorations="Underline">TICKETS</ion-label></h2>\n  <hr class="logohr">\n  <br>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n            pullingIcon="arrow-dropdown"\n            pullingText="Pull to refresh"\n            refreshingSpinner="circles"\n            refreshingText="Loading...">\n    </ion-refresher-content>\n  </ion-refresher>\n  <!--<div class="PicRating">\n    <div class="prfpic" align="left">\n      <div  style="padding-top:4px; padding-left:-2px;" class="cardlabel2P2">\n\n        <ion-img style="margin-left:13px; background-color:green;" class="onlineImg"></ion-img>\n        <ion-img style="margin-left:-8px;" class="profilerating" src="{{(this.userInfo.avatar==\'\')?\'assets/avatar.png\':this.userInfo.avatar}}"></ion-img>\n\n        <ion-label class="profiletexts" >{{this.userInfo.name}}</ion-label>\n        <ion-label class="profiletexts">{{this.userInfo.phone}}</ion-label>\n      </div>\n    </div>\n    <div class="prating" align="right">\n      <div class="prating">\n        <ion-icon name="star" *ngIf="this.userInfo.rating > 0"></ion-icon>\n        <ion-icon name="star" *ngIf="this.userInfo.rating > 1"></ion-icon>\n        <ion-icon name="star" *ngIf="this.userInfo.rating > 2"></ion-icon>\n        <ion-icon name="star" *ngIf="this.userInfo.rating > 3"></ion-icon>\n        <ion-icon name="star" *ngIf="this.userInfo.rating > 4"></ion-icon>\n        <ion-icon name="star-outline" *ngIf="this.userInfo.rating < 1"></ion-icon>\n        <ion-icon name="star-outline" *ngIf="this.userInfo.rating < 2"></ion-icon>\n        <ion-icon name="star-outline" *ngIf="this.userInfo.rating < 3"></ion-icon>\n        <ion-icon name="star-outline" *ngIf="this.userInfo.rating < 4"></ion-icon>\n        <ion-icon name="star-outline" *ngIf="this.userInfo.rating < 5"></ion-icon>\n      </div>\n    </div>\n  </div>\n  <br>-->\n\n  <ion-list class="ticket-list">\n    <ion-item class="item item-text-wrap text-wrap" *ngFor="let ticket of history" (click)="ticketclick(ticket.ticket_id,ticket.approved,ticket.screen_id)" >\n      <ion-avatar item-start>\n        <img src="{{(ticket.logo==\'\')?\'assets/avatar.png\':ticket.logo}}">\n      </ion-avatar>\n      <p class="ticket-status"><span ion-text [style.color]="ticket.color">{{ticket.status}} </span>  - {{ticket.org_name}} </p>\n      <p class="ticket-desc-list"> {{ticket.problem_desc}}</p>\n      <p>{{ ticket.created_at | date: \'dd-M-yyyy\'}}</p>\n    </ion-item>\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/tickets/tickets.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */]])
    ], TicketsPage);
    return TicketsPage;
}());

//# sourceMappingURL=tickets.js.map

/***/ }),

/***/ 135:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 135;

/***/ }),

/***/ 177:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/chat-details/chat-details.module": [
		306,
		18
	],
	"../pages/chat/chat.module": [
		307,
		17
	],
	"../pages/clientsign/clientsign.module": [
		308,
		16
	],
	"../pages/dashboard/dashboard.module": [
		309,
		15
	],
	"../pages/history/history.module": [
		310,
		14
	],
	"../pages/jobdone/jobdone.module": [
		311,
		13
	],
	"../pages/jobreview/jobreview.module": [
		312,
		12
	],
	"../pages/previoustickets/previoustickets.module": [
		313,
		11
	],
	"../pages/profile/profile.module": [
		314,
		10
	],
	"../pages/remotearea/remotearea.module": [
		315,
		9
	],
	"../pages/resetpassword/resetpassword.module": [
		316,
		8
	],
	"../pages/reviewtech/reviewtech.module": [
		317,
		7
	],
	"../pages/startrepair/startrepair.module": [
		318,
		6
	],
	"../pages/takephoto/takephoto.module": [
		319,
		5
	],
	"../pages/techbooked/techbooked.module": [
		320,
		4
	],
	"../pages/techrequest/techrequest.module": [
		321,
		3
	],
	"../pages/tickets/tickets.module": [
		322,
		2
	],
	"../pages/ticketupdate/ticketupdate.module": [
		323,
		1
	],
	"../pages/trb-shoot/trb-shoot.module": [
		324,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 177;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobreviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the JobreviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var JobreviewPage = (function () {
    function JobreviewPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    JobreviewPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad JobreviewPage');
    };
    JobreviewPage.prototype.submit = function () {
        this.navCtrl.popAll();
    };
    JobreviewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-jobreview',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/jobreview/jobreview.html"*/'<!--\n  Generated template for the JobreviewPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>JOB REVIEW</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content align="center" padding>\n  <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n  <hr class="logohr">\n  <h2 class="imp"><ion-label align="center" TextDecorations="Underline">TECHNICIAN REQUEST</ion-label>\n  </h2><hr class="logohr">\n  <br><br>\n  \n  <br><br>\n  <button  round class="ticketUpdate" align="center" (click)="submit()">SUBMIT</button>\n</ion-content>\n\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/jobreview/jobreview.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], JobreviewPage);
    return JobreviewPage;
}());

//# sourceMappingURL=jobreview.js.map

/***/ }),

/***/ 227:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(248);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 248:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_push__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angular2_signaturepad__ = __webpack_require__(303);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angular2_signaturepad___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_angular2_signaturepad__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_network__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_component__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_home_home__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_dashboard_dashboard__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_chat_chat__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_chat_details_chat_details__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_history_history__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_profile_profile__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_remotearea_remotearea__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_ticketupdate_ticketupdate__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_takephoto_takephoto__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_reviewtech_reviewtech__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_clientsign_clientsign__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_techrequest_techrequest__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_techbooked_techbooked__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_resetpassword_resetpassword__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_startrepair_startrepair__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_jobdone_jobdone__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_jobreview_jobreview__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_previoustickets_previoustickets__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_trb_shoot_trb_shoot__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__angular_common_http__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__providers_rest_rest__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33_angular2_signaturepad_signature_pad__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33_angular2_signaturepad_signature_pad___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_33_angular2_signaturepad_signature_pad__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_tickets_tickets__ = __webpack_require__(121);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



































var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_12__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_dashboard_dashboard__["a" /* DashboardPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_history_history__["a" /* HistoryPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_remotearea_remotearea__["a" /* RemoteareaPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_ticketupdate_ticketupdate__["a" /* TicketupdatePage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_takephoto_takephoto__["a" /* TakephotoPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_reviewtech_reviewtech__["a" /* ReviewtechPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_clientsign_clientsign__["a" /* ClientsignPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_techrequest_techrequest__["a" /* TechrequestPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_techbooked_techbooked__["a" /* TechbookedPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_resetpassword_resetpassword__["a" /* ResetpasswordPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_startrepair_startrepair__["a" /* StartrepairPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_jobdone_jobdone__["a" /* JobdonePage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_jobreview_jobreview__["a" /* JobreviewPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_previoustickets_previoustickets__["a" /* PreviousticketsPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_chat_details_chat_details__["a" /* ChatDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_trb_shoot_trb_shoot__["a" /* TrbShootPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_tickets_tickets__["a" /* TicketsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/chat-details/chat-details.module#ChatDetailsPageModule', name: 'ChatDetailsPage', segment: 'chat-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/clientsign/clientsign.module#ClientsignPageModule', name: 'ClientsignPage', segment: 'clientsign', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/history/history.module#HistoryPageModule', name: 'HistoryPage', segment: 'history', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/jobdone/jobdone.module#JobdonePageModule', name: 'JobdonePage', segment: 'jobdone', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/jobreview/jobreview.module#JobreviewPageModule', name: 'JobreviewPage', segment: 'jobreview', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/previoustickets/previoustickets.module#PreviousticketsPageModule', name: 'PreviousticketsPage', segment: 'previoustickets', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/remotearea/remotearea.module#RemoteareaPageModule', name: 'RemoteareaPage', segment: 'remotearea', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/resetpassword/resetpassword.module#ResetpasswordPageModule', name: 'ResetpasswordPage', segment: 'resetpassword', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reviewtech/reviewtech.module#ReviewtechPageModule', name: 'ReviewtechPage', segment: 'reviewtech', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/startrepair/startrepair.module#StartrepairPageModule', name: 'StartrepairPage', segment: 'startrepair', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/takephoto/takephoto.module#TakephotoPageModule', name: 'TakephotoPage', segment: 'takephoto', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/techbooked/techbooked.module#TechbookedPageModule', name: 'TechbookedPage', segment: 'techbooked', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/techrequest/techrequest.module#TechrequestPageModule', name: 'TechrequestPage', segment: 'techrequest', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tickets/tickets.module#TicketsPageModule', name: 'TicketsPage', segment: 'tickets', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ticketupdate/ticketupdate.module#TicketupdatePageModule', name: 'TicketupdatePage', segment: 'ticketupdate', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trb-shoot/trb-shoot.module#TrbShootPageModule', name: 'TrbShootPage', segment: 'trb-shoot', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_9_angular2_signaturepad__["SignaturePadModule"],
                __WEBPACK_IMPORTED_MODULE_31__angular_common_http__["b" /* HttpClientModule */],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_12__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_dashboard_dashboard__["a" /* DashboardPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_history_history__["a" /* HistoryPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_remotearea_remotearea__["a" /* RemoteareaPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_ticketupdate_ticketupdate__["a" /* TicketupdatePage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_takephoto_takephoto__["a" /* TakephotoPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_reviewtech_reviewtech__["a" /* ReviewtechPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_clientsign_clientsign__["a" /* ClientsignPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_techrequest_techrequest__["a" /* TechrequestPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_techbooked_techbooked__["a" /* TechbookedPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_resetpassword_resetpassword__["a" /* ResetpasswordPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_startrepair_startrepair__["a" /* StartrepairPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_jobdone_jobdone__["a" /* JobdonePage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_jobreview_jobreview__["a" /* JobreviewPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_previoustickets_previoustickets__["a" /* PreviousticketsPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_chat_details_chat_details__["a" /* ChatDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_trb_shoot_trb_shoot__["a" /* TrbShootPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_tickets_tickets__["a" /* TicketsPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_32__providers_rest_rest__["a" /* RestProvider */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["a" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_9_angular2_signaturepad__["SignaturePadModule"],
                __WEBPACK_IMPORTED_MODULE_33_angular2_signaturepad_signature_pad__["SignaturePad"],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_network__["a" /* Network */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, network, alertCtrl) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            var disconnectSubscription = network.onDisconnect().subscribe(function () {
                var alert = alertCtrl.create({
                    title: 'Offline',
                    subTitle: 'No Internet Connection is available the app will not work',
                    buttons: ['Ok']
                });
                alert.present();
            });
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__["a" /* Network */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TicketupdatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__takephoto_takephoto__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the TicketupdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TicketupdatePage = (function () {
    function TicketupdatePage(navCtrl, navParams, toastCtrl, restProvider, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.restProvider = restProvider;
        this.alertCtrl = alertCtrl;
        this.restProvider.getAllStatuses().then(function (result) {
            _this.status = result;
        });
    }
    TicketupdatePage.prototype.next = function () {
        var _this = this;
        if (this.selected_status == '' || this.message == '') {
            var alert_1 = this.alertCtrl.create({
                title: 'Enter Dtaa',
                subTitle: 'Please select status and enter message before continuing',
                buttons: ['Dismiss']
            });
            alert_1.present();
            return;
        }
        this.restProvider.stopTicket(this.navParams.get('ticket_id'), this.message, this.selected_status).then(function (result) {
            _this.presentToast(result['msg']);
        });
    };
    TicketupdatePage.prototype.presentToast = function (msg) {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__takephoto_takephoto__["a" /* TakephotoPage */], { 'ticket_id': _this.navParams.get('ticket_id') });
        });
        toast.present();
    };
    TicketupdatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ticketupdate',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/ticketupdate/ticketupdate.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>TICKET UPDATE</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div align="center">\n      <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n      <hr class="logohr">\n      <h2 class="imp"><ion-label align="center" TextDecorations="Underline">TICKET UPDATE</ion-label>\n      </h2><hr class="logohr"><br>\n      <ion-img style="padding-top:6px;" align="center" class="ticketImg" src="assets/imgs/updateimg.png"></ion-img>\n  \n  </div>  \n    <br><br><br>\n  <ion-item style="border: 0.5px solid black; padding-left:0px;" class="multiPicker">\n    <ion-label style="padding-left:15px;" placeholder="Status">Status</ion-label>\n    <ion-select [(ngModel)]="selected_status">\n      <ion-option *ngFor="let stat of status" [value]="stat.stat_id">{{stat.status}}</ion-option>\n    </ion-select>\n  </ion-item>\n\n\n<textarea style="margin-left:18px;" class="ticketDetailsArea" placeholder="Enter Status Description here" [(ngModel)]=\'message\'></textarea>\n<div style="padding-right:17px; padding-top:2px;">\n  <button round class="tecBtn" (click)="next()" >NEXT</button>\n\n</div>\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/ticketupdate/ticketupdate.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], TicketupdatePage);
    return TicketupdatePage;
}());

//# sourceMappingURL=ticketupdate.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TakephotoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__reviewtech_reviewtech__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the TakephotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TakephotoPage = (function () {
    function TakephotoPage(navCtrl, navParams, camera, restProvider, toastCtrl, loadingCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.photo = "assets/imgs/photo.png";
    }
    TakephotoPage.prototype.ionViewDidLoad = function () {
    };
    TakephotoPage.prototype.takePicture = function () {
        var _this = this;
        var options = {
            quality: 80,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.CAMERA,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            targetWidth: 2048,
            targetHeight: 1080,
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.photo = 'data:image/jpeg;base64,' + imageData;
            _this.imgdata = imageData;
        }, function (err) {
            // Handle error
        });
    };
    TakephotoPage.prototype.presentToast = function (msg) {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__reviewtech_reviewtech__["a" /* ReviewtechPage */], { 'ticket_id': _this.navParams.get('ticket_id') });
        });
        toast.present();
    };
    TakephotoPage.prototype.next = function () {
        var _this = this;
        if (this.imgdata === undefined) {
            var alert_1 = this.alertCtrl.create({
                title: 'Add Photo',
                subTitle: 'Please take photo before moving next',
                buttons: ['Dismiss']
            });
            alert_1.present();
            return;
        }
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.restProvider.savePhoto(this.navParams.get('ticket_id'), this.imgdata).then(function (result) {
            _this.presentToast(result['msg']);
            loading.dismiss();
        });
    };
    TakephotoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-takephoto',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/takephoto/takephoto.html"*/'<!--\n  Generated template for the TakephotoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>TICKET TAKEPHOTO</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content align="center" padding>\n    <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n    <hr class="logohr">\n    <h2><ion-label align="center" TextDecorations="Underline">TICKET UPDATE</ion-label>\n    </h2><hr class="logohr">\n  \n    <ion-card>\n      <div align="center" style="padding-top: 40px;">\n          <ion-img  align="center"  style="padding-top: 40px;" class="ticketfile"  src="{{photo}}"></ion-img>\n  \n      </div>\n    </ion-card><br>\n    <ion-img align="center"  class="capture" src="assets/circle.png" (click)="takePicture()" ></ion-img>\n    <br><br>\n\n    <button round class="nextBtn" (click)="next()" >NEXT</button>\n\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/takephoto/takephoto.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], TakephotoPage);
    return TakephotoPage;
}());

//# sourceMappingURL=takephoto.js.map

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TechbookedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__startrepair_startrepair__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__ = __webpack_require__(182);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the TechbookedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TechbookedPage = (function () {
    function TechbookedPage(alertCtrl, navCtrl, navParams, toastCtrl, geolocation, restProvider, googleMaps, loadingCtrl) {
        var _this = this;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.geolocation = geolocation;
        this.restProvider = restProvider;
        this.googleMaps = googleMaps;
        this.loadingCtrl = loadingCtrl;
        this.loading = this.loadingCtrl.create({
            content: 'Getting location...'
        });
        this.loading.present();
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.loading.dismiss();
            _this.latitude = resp.coords.latitude;
            _this.longitude = resp.coords.longitude;
            _this.restProvider.getTicketDetails(_this.navParams.get('ticket_id'), resp.coords.latitude, resp.coords.longitude).then(function (result) {
                _this.ticket_id = result['ticket'].ticket_id;
                _this.address = result['ticket'].address;
                _this.logo = result['ticket'].logo;
                _this.organization = result['ticket'].org_name;
                _this.screen_id = result['ticket'].screen_id;
                _this.phone = result['ticket'].phone;
                _this.distance = result['distance'];
                _this.duration = result['duration'];
                _this.loadMap(result['ticket'].latitude, result['ticket'].longitude);
                _this.map.addMarker({
                    title: 'Support',
                    icon: 'assets/tech-map-icon.png',
                    animation: 'DROP',
                    position: {
                        lat: _this.latitude,
                        lng: _this.longitude,
                    }
                });
                _this.map.addMarker({
                    title: 'Screen',
                    icon: 'assets/client-map-icon.png',
                    // animation: 'DROP',
                    position: {
                        lat: result['ticket'].latitude,
                        lng: result['ticket'].longitude,
                    }
                });
            });
            // resp.coords.latitude
            // resp.coords.longitude
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    }
    TechbookedPage.prototype.ionViewDidLoad = function () {
        //do nothing
    };
    TechbookedPage.prototype.loadMap = function (lati, long) {
        var mapOptions = {
            camera: {
                target: {
                    lat: lati,
                    lng: long,
                },
                zoom: 12,
                tilt: 30
            }
        };
        var element = document.getElementById('map');
        this.map = __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["a" /* GoogleMaps */].create(element, mapOptions);
        // this.map = GoogleMaps.create('map', mapOptions);
        // Wait the MAP_READY before using any methods.
        this.map.one(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MAP_READY)
            .then(function () {
            // Now you can use all methods safely.
        });
    };
    TechbookedPage.prototype.next = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__startrepair_startrepair__["a" /* StartrepairPage */], { 'ticket_id': this.navParams.get('ticket_id'), 'screen_id': this.navParams.get('screen_id') });
    };
    TechbookedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-techbooked',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/techbooked/techbooked.html"*/'<!--\n  Generated template for the TechbookedPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Ticket Details</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content align="center" padding>\n  <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n  <hr class="logohr">\n  <h2 class="techbook"><ion-label align="center" TextDecorations="Underline">TECHNICIAN REQUEST</ion-label>\n  </h2><hr class="logohr">\n  <br>\n  <div class="information">Estimated time {{duration}}</div>\n  <div class="mapImage" id="map" style="height: 60%">\n    <!--<ion-img class="mapImage" align="center" src="assets/imgs/map.png"></ion-img>-->\n  </div>\n  <div align="center" >\n    <ion-label style="font-size:20px; font-weight:300;">Client Info</ion-label>\n  </div>\n  <br>\n  \n  <div class="CardImageLabelCSS">\n    <div class="CardImageCSS">\n      \n      <ion-img class="vodaImage" src="{{(logo==\'\')?\'assets/avatar.png\':logo}}"></ion-img>\n    </div>\n    <div class="CardLabelCSS">\n        <ion-label style="float:left; margin-top:7px; line-height: 14px; font-size:14px;" class="vodaLabels">{{organization}}</ion-label>\n        <ion-label style="float:left; margin-top:2px; line-height: 12px; font-size:14px;" class="vodaLabels">{{phone}}</ion-label>\n     </div>\n    </div>\n<br>\n    <button  round class="ticketUpdate" align="center" (click) = "next()">READY TO START? </button>\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/techbooked/techbooked.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["a" /* GoogleMaps */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]])
    ], TechbookedPage);
    return TechbookedPage;
}());

//# sourceMappingURL=techbooked.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobdonePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__previoustickets_previoustickets__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ticketupdate_ticketupdate__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the JobdonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var JobdonePage = (function () {
    function JobdonePage(actionSheetCtrl, navCtrl, navParams, toastCtrl, restProvider) {
        this.actionSheetCtrl = actionSheetCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.restProvider = restProvider;
        //do nothing here
    }
    JobdonePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad JobdonePage');
    };
    JobdonePage.prototype.jobDone = function () {
        /*this.restProvider.stopTicket(this.navParams.get('ticket_id')).then((result)=>{
            this.presentToast(result['msg']);
        });*/
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__ticketupdate_ticketupdate__["a" /* TicketupdatePage */], { 'ticket_id': this.navParams.get('ticket_id') });
    };
    JobdonePage.prototype.presentToast = function (msg) {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            _this.navCtrl.pop();
        });
        toast.present();
    };
    JobdonePage.prototype.ticketHistoryfunc = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__previoustickets_previoustickets__["a" /* PreviousticketsPage */], { 'screen_id': this.navParams.get('screen_id') });
    };
    JobdonePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-jobdone',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/jobdone/jobdone.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>JOB DONE</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content align="center" padding>\n  <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n  <hr class="logohr">\n  <h2 class="imp"><ion-label align="center" TextDecorations="Underline">TECHNICIAN REQUEST</ion-label>\n  </h2><hr class="logohr">\n  <br>\n  <div>\n    <ion-img style="padding-top:6px;" class="repairImage" align="center" src="assets/imgs/done.png"></ion-img>\n  </div>\n<br>\n  <button  round class="ticketUpdate" align="center" (click) = "jobDone()">Job Done</button>\n    <br><br>\n  <button  round class="ticketUpdate" align="center" (click) = "ticketHistoryfunc()">PREVIOUS TICKETS</button>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/jobdone/jobdone.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */]])
    ], JobdonePage);
    return JobdonePage;
}());

//# sourceMappingURL=jobdone.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TechrequestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__techbooked_techbooked__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the TechrequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TechrequestPage = (function () {
    function TechrequestPage(alertCtrl, navCtrl, navParams, toastCtrl, geolocation, restProvider) {
        var _this = this;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.geolocation = geolocation;
        this.restProvider = restProvider;
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.latitude = resp.coords.latitude;
            _this.longitude = resp.coords.longitude;
            _this.restProvider.getTicketDetails(_this.navParams.get('ticket_id'), resp.coords.latitude, resp.coords.longitude).then(function (result) {
                _this.ticket_id = result['ticket'].ticket_id;
                _this.address = result['ticket'].address;
                _this.logo = result['ticket'].logo;
                _this.organization = result['ticket'].org_name;
                _this.screen_id = result['ticket'].screen_id;
                _this.distance = result['distance'];
                _this.duration = result['duration'];
            });
            // resp.coords.latitude
            // resp.coords.longitude
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    }
    TechrequestPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TechrequestPage');
    };
    TechrequestPage.prototype.techRequest = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__techbooked_techbooked__["a" /* TechbookedPage */]);
    };
    TechrequestPage.prototype.reject = function () {
        var _this = this;
        {
            var alert_1 = this.alertCtrl.create({
                title: 'Confirm',
                message: 'Do you want to Reject Request?',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function () {
                            console.log('cancel clicked');
                        }
                    },
                    {
                        text: 'OK',
                        role: 'OK',
                        handler: function () {
                            _this.restProvider.declineTicket(_this.navParams.get('ticket_id')).then(function (result) {
                                _this.presentToast(result['msg']);
                            }, function (err) {
                                console.log(err);
                            });
                        }
                    }
                ]
            });
            alert_1.present();
        }
    };
    TechrequestPage.prototype.presentToast = function (msg) {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            _this.navCtrl.pop();
        });
        toast.present();
    };
    TechrequestPage.prototype.accept = function () {
        var _this = this;
        {
            var alert_2 = this.alertCtrl.create({
                title: 'Confirm',
                message: 'Do you want to Accept Request?',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function () {
                            console.log('cancel clicked');
                        }
                    },
                    {
                        text: 'OK',
                        role: 'OK',
                        handler: function () {
                            _this.restProvider.acceptTicket(_this.navParams.get('ticket_id'), _this.latitude, _this.longitude).then(function (result) {
                                _this.presentToast(result['msg']);
                            }, function (err) {
                                console.log(err);
                            });
                        }
                    }
                ]
            });
            alert_2.present();
        }
    };
    TechrequestPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-techrequest',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/techrequest/techrequest.html"*/'<!--\n  Generated template for the TechrequestPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>techrequest</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content align="center" padding>\n    <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n    <hr class="logohr">\n    <h2 class="imp"><ion-label align="center" TextDecorations="Underline">Ticket # {{ticket_id}}</ion-label>\n    </h2><hr class="logohr">\n    <br>\n    <div align="center" class="imgVoda" >\n    <ion-img  class="imgVoda" src="{{(logo==\'\')?\'assets/avatar.png\':logo}}"></ion-img>\n  </div >\n    <div style="margin-top:-10px;" class="labelVodafone">\n      <ion-label style="padding-left:-2px;" align="center">{{organization}}</ion-label>\n      <ion-label style="margin-top:-8px;padding-left:-2px;">Screen ID: {{screen_id}}</ion-label>\n    </div>\n\n    <div>\n      <ion-card class="backshadow">\n        <ion-label style="float:left; margin-left:12px;">Address: {{address}}</ion-label>\n      </ion-card>\n    </div>\n    <div>\n      <div></div>\n\n      <div class="labeltext1">\n        <div class="labeltext1p1">\n          <ion-img  class="labelImage" src="assets/imgs/done.png"></ion-img>\n        </div>\n        <div style="padding-top: 9px;" class="labeltext1p2">\n          <ion-label style="float:left; margin-top:9px; line-height: 16px; font-size:14px;"  >Estimated Distance {{distance}}</ion-label>\n        <ion-label style="float:left; margin-top:-5px; line-height: 16px; font-size:14px;">Estimated time for arrival {{duration}}</ion-label>\n        </div>\n      </div>\n    </div>\n    <div class="actionCircle" style="padding-top:5px;">\n      <div style=" width: 56%;\n      height: 90px;" style="padding-left:20px;"  class="decline circle-decline" align="left">\n          <a (click)="reject()"><ion-img  class="circleImage" src="assets/imgs/reject.png"></ion-img></a>\n      </div>\n      <div style="width: 56%;\n      height: 90px;" style="padding-right:20px;" class="accept circle-accept" align="right">\n          <a (click)="accept()"><ion-img class="circleImage" src="assets/imgs/accept.png"></ion-img></a>\n      </div>\n    </div>\n    <!--<br>-->\n    <!--<br>-->\n    <!--<button  round class="ticketUpdate" align="center" (click) = "techRequest()">NEXT</button>-->\n  </ion-content>\n\n  <!--\n\n\n  -->\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/techrequest/techrequest.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */]])
    ], TechrequestPage);
    return TechrequestPage;
}());

//# sourceMappingURL=techrequest.js.map

/***/ }),

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PreviousticketsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PreviousticketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PreviousticketsPage = (function () {
    function PreviousticketsPage(navCtrl, navParams, restProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.restProvider.getScreenHistory(navParams.get('screen_id')).then(function (result) {
            _this.history = result;
        });
    }
    PreviousticketsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PreviousticketsPage');
    };
    PreviousticketsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-previoustickets',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/previoustickets/previoustickets.html"*/'<!--\n  Generated template for the PreviousticketsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>previoustickets</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content align="center" padding>\n    <ion-img class="Logo" align="center" src="assets/kitvision.png"></ion-img>\n    <hr class="logohr">\n    <h2 class="imp"><ion-label align="center" TextDecorations="Underline">PREVIOUS REQUEST</ion-label>\n    </h2><hr class="logohr">\n    <br>\n\n    <ion-card class="cardContainer" *ngFor="let ticket of history">\n        <ion-card-header class="cardheader-h">\n            <div class="cardlabel">\n                <ion-label class="ticket"><font color="red">Ticket ID#{{ticket.ticket_id}}</font></ion-label>\n                <ion-label class="date">{{ ticket.created_at | date: \'dd-M-yyyy\'}}</ion-label>\n            </div>\n            <hr class="hrLine">\n            <div class="cardlabel2">\n                <div class="cardlabel2P1">\n                    <ion-label class="labelfirst">{{ticket.screen_name}}</ion-label>\n                    <ion-label style="margin-left:24px;" class="status">{{ticket.status}}</ion-label>\n                </div>\n                <hr class="hrLine">\n            </div>\n            <div class="labelfirsts-h" >\n                <ion-label item-start class="labelfirsts">{{ticket.org_name}}</ion-label>\n            </div>\n            <hr class="hrLine">\n            <div style="padding-top:-5px;" class="personDetails-h">\n                <ion-label class="ratingLabel">Rating</ion-label>\n                <div class="rating">\n                    <ion-icon name="star" *ngIf="ticket.rating > 0"></ion-icon>\n                    <ion-icon name="star" *ngIf="ticket.rating > 1"></ion-icon>\n                    <ion-icon name="star" *ngIf="ticket.rating > 2"></ion-icon>\n                    <ion-icon name="star" *ngIf="ticket.rating > 3"></ion-icon>\n                    <ion-icon name="star" *ngIf="ticket.rating > 4"></ion-icon>\n                    <ion-icon name="star-outline" *ngIf="ticket.rating < 1"></ion-icon>\n                    <ion-icon name="star-outline" *ngIf="ticket.rating < 2"></ion-icon>\n                    <ion-icon name="star-outline" *ngIf="ticket.rating < 3"></ion-icon>\n                    <ion-icon name="star-outline" *ngIf="ticket.rating < 4"></ion-icon>\n                    <ion-icon name="star-outline" *ngIf="ticket.rating < 5"></ion-icon>\n                </div>\n            </div>\n            <hr class="hrLine">\n        </ion-card-header>\n        <ion-card-content class="cardContent">\n            <div style="margin-top:-18px;" class="cardContent">\n                <div class="message">\n                    <p>Main Message</p>\n                </div>\n                <div class="messagedetails">\n                    <p>{{ticket.problem_desc}}</p>\n                </div>\n            </div>\n        </ion-card-content>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/previoustickets/previoustickets.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
    ], PreviousticketsPage);
    return PreviousticketsPage;
}());

//# sourceMappingURL=previoustickets.js.map

/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__resetpassword_resetpassword__ = __webpack_require__(118);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = (function () {
    function HomePage(navCtrl, restProvider, toastCtrl) {
        /* this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        if(this.currentUserData.token==null)
        {
          
        }
        else
        {
          this.navCtrl.push(DashboardPage);
        }*/
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.credential = { username: '', password: '' };
    }
    HomePage.prototype.verifyLogin = function () {
        var _this = this;
        if (this.credential.username == '' || this.credential.password == '') {
            this.presentToast('Please enter username and password');
            return;
        }
        this.restProvider.login(this.credential).then(function (result) {
            if (result['error']) {
                _this.presentToast(result['error']);
            }
            else {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
            }
        }, function (err) {
            console.log(err);
        });
        //this.navCtrl.push(DashboardPage);
    };
    HomePage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });
        toast.present();
    };
    HomePage.prototype.reset = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__resetpassword_resetpassword__["a" /* ResetpasswordPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/admin/Desktop/KitvisionTech/src/pages/home/home.html"*/'<ion-content align="center" hide-tabs>\n\n<br><br><br><br><br><br><br><br><br>\n    <ion-img  align="center" class="Logo" src="assets/kitvision.png"></ion-img>\n    <br><br><br><br>\n  <p>{{ alerts }} </p>\n      <ion-input class="input" placeholder="UserName" [(ngModel)]="credential.username"></ion-input>\n   \n      <ion-input type="password" placeholder="Password" [(ngModel)]="credential.password"></ion-input>\n    \n  <br/>\n  <button  round class="buttonLogin" align="center" (click) = "verifyLogin()">LOGIN</button>\n  <br/>\n    <a (click)="reset()">\n        <u><ion-label class="forgetPassword">Forget Your Password!</ion-label></u>\n    </a>\n</ion-content>\n  '/*ion-inline-end:"/Users/admin/Desktop/KitvisionTech/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

},[227]);
//# sourceMappingURL=main.js.map