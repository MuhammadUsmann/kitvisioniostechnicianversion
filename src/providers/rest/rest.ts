import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Geolocation } from "@ionic-native/geolocation";
import {tick} from "@angular/core/testing";

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
    apiUrl = 'http://app.kitvision.org/api';
    public token: string;
    public username: string;
    public subscribeGPS:any;

    constructor(public http: HttpClient) {
        console.log('Hello RestProvider Provider');
    }

    login(data){
        this.username = data.username;
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/login/tech', JSON.stringify(data))
                .subscribe(res => {
                    // login successful if there's a jwt token in the response
                    const token = res['token'];
                    if (token) {
                        // set token property
                        this.token = token;
                        var permissions = [];
                        for(let data of res['permissions']) {
                            permissions.push(data.permission);
                        }

                        // store username and jwt token in local storage to keep user logged in between page refreshes
                        localStorage.setItem('currentUser', JSON.stringify({ "username":this.username, "token":this.token,"permissions":permissions.join(',') }));
                        localStorage.setItem('userInfo', JSON.stringify({"avatar":res['user'].avatar , "phone":res['user'].phone,"name":res['user'].name,'rating':res['user'].rating}));

                        this.subscribeGPS = new Geolocation().watchPosition({ maximumAge: 30000, timeout: 10000, enableHighAccuracy: true }).subscribe(position => {
                            console.log(position.coords.latitude);
                                this.setLoc(this.token,position.coords.latitude,position.coords.longitude ).then((result)=>{
                                    //do nothing
                                });

                            });

                        // return true to indicate successful login
                        resolve(res);
                    } else {
                        // return false to indicate failed login
                        resolve(res);
                    }
                    //console.log(res['token']);
                    resolve(res);

                }, (err) => {
                    reject(err);
                });
        });

    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
        localStorage.removeItem('userInfo');
        this.subscribeGPS.unsubscribe();
    }

    resetpwd(email) {
        const body = new HttpParams()
            .set('email', email);
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/reset', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    setLoc(token,lat,long)
    {
        //console.log(token);
        const body = new HttpParams()
            .set('lat',lat)
            .set('long',long)
            .set('api_token', token);
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/location/set', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    //console.log(res);
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });

    }

    getFiles(token)
    {
        //console.log(token);
        const body = new HttpParams()
            .set('api_token', token)
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/files/troubleshoot', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    //console.log(res);
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    getHistory(token)
    {
        //console.log(token);
        const body = new HttpParams()
            .set('api_token', token)
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/all', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    console.log(res);
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    getTickets(token)
    {
        //console.log(token);
        const body = new HttpParams()
            .set('api_token', token)
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/assigned', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    console.log(res);
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    getRemote()
    {
        //console.log(token);
        const body = new HttpParams()
            .set('api_token', this.token);
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/remote/all', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    console.log(res);
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    changePassword(data) {
        const body = new HttpParams()
            .set('password', data.password)
            .set('api_token',this.token)
            .set('con_password', data.con_password);
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/user/changepwd', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    changePhone(phone) {
        const body = new HttpParams()
            .set('api_token',this.token)
            .set('phone', phone);
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/user/changePhone', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    getAllChats(){
        const body = new HttpParams()
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/chat/all', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    console.log(res);
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }
    getChatStatus(){
        const body = new HttpParams()
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/chat/status/get', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });

    }

    setChatStatus(status){
        if(this.token == null)
            return;

        const body = new HttpParams()
            .set('client','0')
            .set('status',status)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/chat/status/set', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });

    }

    getMessages(thread_id){
        const body = new HttpParams()
            .set('thread_id',thread_id)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/chat/get', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });

    }

    sendMessage(message,client_id, thread_id){
        const body = new HttpParams()
            .set('message',message)
            .set('thread_id',thread_id)
            .set('client_id',client_id)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/chat/sendMessage', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });

    }
    getTicketDetails(ticket_id,lat,long){

    console.log("TicketId: ",ticket_id,"Lati: ", lat,"Long: ",long);
        const body = new HttpParams()
            .set('ticket_id',ticket_id)
            .set('latitude',lat)
            .set('longitude',long)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/getTicket', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });

    }

    declineTicket(ticket_id){
        const body = new HttpParams()
            .set('ticket_id',ticket_id)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/decline', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }
    acceptTicket(ticket_id,lat, long){
        const body = new HttpParams()
            .set('ticket_id',ticket_id)
            .set('latitude',lat)
            .set('longitude',long)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/accept', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    startTicket(ticket_id,lat, long){
        const body = new HttpParams()
            .set('ticket_id',ticket_id)
            .set('latitude',lat)
            .set('longitude',long)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/start', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    stopTicket(ticket_id,message,status_id){
        const body = new HttpParams()
            .set('ticket_id',ticket_id)
            .set('message',message)
            .set('status',status_id)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/stop', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    getScreenHistory(screen_id) {
        const body = new HttpParams()
            .set('screen_id', screen_id)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/screen/all', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    getAllStatuses() {
        const body = new HttpParams()
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/status/all', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    savePhoto(ticket_id,img_data){
        const body = new HttpParams()
            .set('ticket_id',ticket_id)
            .set('image_data',img_data)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/savePhoto', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    saveSign(ticket_id,img_data){
        const body = new HttpParams()
            .set('ticket_id',ticket_id)
            .set('image_data',img_data)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/saveSignature', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    saveAnswer(ticket_id,questions_data){
        const body = new HttpParams()
            .set('ticket_id',ticket_id)
            .set('question_data',JSON.stringify(questions_data))
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/answers', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    getQuestions(){
        const body = new HttpParams()
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/ticket/survey', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }

    setChatToken(token_id){
        const body = new HttpParams()
            .set('chat_token',token_id)
            .set('api_token',this.token);

        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'/chat/setToken', body.toString(),{
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
        });
    }
}
