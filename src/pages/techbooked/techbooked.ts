import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';
import { StartrepairPage } from '../startrepair/startrepair';
import {Geolocation} from "@ionic-native/geolocation";
import {RestProvider} from "../../providers/rest/rest";
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    GoogleMapOptions,
    CameraPosition,
    MarkerOptions,
    Marker, GoogleMapsAnimation
} from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-techbooked',
  templateUrl: 'techbooked.html',
})
export class TechbookedPage {
    map: GoogleMap;
    loading:any;

    ticket_id:string;
    address:string;
    logo:string;
    organization:string;
    screen_id:string;
    distance:string;
    duration:string;
    latitude:any;
    longitude:any;
    phone:string;

    constructor(private alertCtrl: AlertController,public navCtrl: NavController,
                public navParams: NavParams, private toastCtrl:ToastController,
                private geolocation:Geolocation, private restProvider:RestProvider,
                private googleMaps:GoogleMaps, private loadingCtrl: LoadingController) {

                console.log('ImConstrustor');

        this.loading = this.loadingCtrl.create({
            content: 'Getting location...'
        });

        this.loading.present();


  this.geolocation.getCurrentPosition().then((resp) => {
  console.log("Resp", resp);
  this.loading.dismiss();
            this.latitude = resp.coords.latitude;
            this.longitude = resp.coords.longitude;

            console.log(this.latitude,this.longitude);

  this.restProvider.getTicketDetails(this.navParams.get('ticket_id'),resp.coords.latitude, resp.coords.longitude).then(result =>{

                console.log("Result", result);
                this.ticket_id = result['ticket'].ticket_id;
                this.address = result['ticket'].address;
                this.logo = result['ticket'].logo;
                this.organization = result['ticket'].org_name;
                this.screen_id = result['ticket'].screen_id;
                this.phone = result['ticket'].phone;
                this.distance = result['distance'];
                this.duration = result['duration'];
                console.log('Result', result);


                this.loadMap(result['ticket'].latitude , result['ticket'].longitude);
                this.map.addMarker({
                    title: 'Support',
                    icon: 'assets/tech-map-icon.png',
                    animation: 'DROP',
                    position: {
                        lat: this.latitude,
                        lng: this.longitude,
                    }
                });

                this.map.addMarker({
                    title: 'Screen',
                    icon: 'assets/client-map-icon.png',
                    // animation: 'DROP',
                    position: {
                        lat: result['ticket'].latitude,
                        lng: result['ticket'].longitude,
                    }
                });

  }).catch(err =>{
    console.log("Error Is: ", JSON.stringify(err) );
  }).catch(err =>{
      console.log("Error Getting Location!", err)
  })
  
    })

    }

  ionViewDidLoad() {
      //do nothing
  }

  loadMap(lati,long){
      let mapOptions: GoogleMapOptions = {
          camera: {
              target: {
                  lat: lati,
                  lng: long,
              },
              zoom: 12,
              tilt: 30
          }
      };

      let element: HTMLElement = document.getElementById('map');
      this.map = GoogleMaps.create(element,mapOptions);
      // this.map = GoogleMaps.create('map', mapOptions);

      // Wait the MAP_READY before using any methods.
      this.map.one(GoogleMapsEvent.MAP_READY)
          .then(() => {


          });
  }

  next(){
    this.navCtrl.push(StartrepairPage,{'ticket_id':this.navParams.get('ticket_id'), 'screen_id':this.navParams.get('screen_id')});
  }

}
