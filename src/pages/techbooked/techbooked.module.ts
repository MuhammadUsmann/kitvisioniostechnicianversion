import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TechbookedPage } from './techbooked';

@NgModule({
  declarations: [
    TechbookedPage,
  ],
  imports: [
    IonicPageModule.forChild(TechbookedPage),
  ],
})
export class TechbookedPageModule {}
