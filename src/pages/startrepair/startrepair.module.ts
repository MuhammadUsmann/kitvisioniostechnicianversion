import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StartrepairPage } from './startrepair';

@NgModule({
  declarations: [
    StartrepairPage,
  ],
  imports: [
    IonicPageModule.forChild(StartrepairPage),
  ],
})
export class StartrepairPageModule {}
