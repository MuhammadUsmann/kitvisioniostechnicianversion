import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { JobdonePage } from '../jobdone/jobdone';
import {Geolocation} from "@ionic-native/geolocation";
import { RestProvider } from "../../providers/rest/rest";
import {PreviousticketsPage} from "../previoustickets/previoustickets";

/**
 * Generated class for the StartrepairPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-startrepair',
  templateUrl: 'startrepair.html',
})
export class StartrepairPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private restProvider: RestProvider, private geolocation: Geolocation, private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StartrepairPage');
  }


  next(){
    this.navCtrl.push(JobdonePage);
  }

  startRepair(){
      this.geolocation.getCurrentPosition().then((resp) => {
          this.restProvider.startTicket(this.navParams.get('ticket_id'),resp.coords.latitude, resp.coords.longitude).then((result)=>{
              this.presentToast(result['msg']);
          });

      }).catch((error) => {
          //this.presentToast('Location not found. Please try again');
      });
  }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position:'top'
        });
        toast.onDidDismiss(() => {
            this.navCtrl.popToRoot();
        });
        toast.present();
    }

    prevHistory(){
        this.navCtrl.push(PreviousticketsPage,{'screen_id':this.navParams.get('screen_id')});
    }



}
