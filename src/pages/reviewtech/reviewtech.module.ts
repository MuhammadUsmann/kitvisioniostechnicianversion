import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReviewtechPage } from './reviewtech';

@NgModule({
  declarations: [
    ReviewtechPage,
  ],
  imports: [
    IonicPageModule.forChild(ReviewtechPage),
  ],
})
export class ReviewtechPageModule {}
