import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { ClientsignPage } from '../clientsign/clientsign';
import { RestProvider } from "../../providers/rest/rest";

/**
 * Generated class for the ReviewtechPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reviewtech',
  templateUrl: 'reviewtech.html',
})
export class ReviewtechPage {
  questions:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private restProvider: RestProvider,
              private toastCtrl: ToastController, private alertCtrl:AlertController) {
    this.restProvider.getQuestions().then((result)=>{
        this.questions = result;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewtechPage');
  }
  nexton(){

  }
  next(){
      var flag = false;
      for(var index=0;index< this.questions.length; index++){
          if(this.questions[index].selected_answer === undefined){
              flag = true;
          }
      }

      if(flag){
          let alert = this.alertCtrl.create({
              title: 'Complete Feedback',
              subTitle: 'Please select all answers before moving next',
              buttons: ['Dismiss']
          });
          alert.present();
          return;
      }

      this.restProvider.saveAnswer(this.navParams.get('ticket_id'),this.questions).then((result)=>{
            this.presentToast(result['msg']);
      });
  }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position:'top'
        });
        toast.onDidDismiss(() => {
            this.navCtrl.push(ClientsignPage,{'ticket_id':this.navParams.get('ticket_id')});
        });
        toast.present();
    }

}
