import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController  } from 'ionic-angular';
import { TechbookedPage } from '../techbooked/techbooked';
import { AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { RestProvider } from "../../providers/rest/rest";

/**
 * Generated class for the TechrequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-techrequest',
  templateUrl: 'techrequest.html',
})
export class TechrequestPage {
  ticket_id:string;
  address:string;
  logo:string;
  organization:string;
  screen_id:string;
  distance:string;
  duration:string;
  latitude:any;
  longitude:any;

  constructor(private alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams, private toastCtrl:ToastController, private geolocation:Geolocation, private restProvider:RestProvider) {
      this.geolocation.getCurrentPosition().then((resp) => {
          this.latitude = resp.coords.latitude;
          this.longitude = resp.coords.longitude;
        this.restProvider.getTicketDetails(this.navParams.get('ticket_id'),resp.coords.latitude, resp.coords.longitude).then((result)=>{
            this.ticket_id = result['ticket'].ticket_id;
            this.address = result['ticket'].address;
            this.logo = result['ticket'].logo;
            this.organization = result['ticket'].org_name;
            this.screen_id = result['ticket'].screen_id;
            this.distance = result['distance'];
            this.duration = result['duration'];
        });
          // resp.coords.latitude
          // resp.coords.longitude
      }).catch((error) => {
          console.log('Error getting location', error);
      });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad TechrequestPage');
  }
  techRequest(){
    this.navCtrl.push(TechbookedPage);
  }
  reject()
  {
    {
      let alert = this.alertCtrl.create({
        title: 'Confirm',
        message: 'Do you want to Reject Request?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('cancel clicked');
            }
            
          },
          {
            text: 'OK',
            role: 'OK',
            handler: () => {
                this.restProvider.declineTicket(this.navParams.get('ticket_id')).then( (result)=>{
                    this.presentToast(result['msg']);
                }, (err) => {
                    console.log(err);
                });
            }
            
          }        
        ]
      });
      alert.present();
    }
  }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });

        toast.onDidDismiss(() => {
            this.navCtrl.pop();
        });

        toast.present();
    }

  accept(){
    {
      let alert = this.alertCtrl.create({
        title: 'Confirm',
        message: 'Do you want to Accept Request?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('cancel clicked');
            }
            
          },
          {
            text: 'OK',
            role: 'OK',
            handler: () => {
                this.restProvider.acceptTicket(this.navParams.get('ticket_id'),this.latitude,this.longitude).then( (result)=>{
                    this.presentToast(result['msg']);
                }, (err) => {
                    console.log(err);
                });
            }
            
          }        
        ]
      });
      alert.present();
    }
  }

}
