import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TechrequestPage } from './techrequest';

@NgModule({
  declarations: [
    TechrequestPage,
  ],
  imports: [
    IonicPageModule.forChild(TechrequestPage),
  ],
})
export class TechrequestPageModule {}
