import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { ReviewtechPage } from '../reviewtech/reviewtech';
import { Camera, CameraOptions } from "@ionic-native/camera";
import { RestProvider } from "../../providers/rest/rest";
import { LoadingController } from "ionic-angular";

/**
 * Generated class for the TakephotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-takephoto',
  templateUrl: 'takephoto.html',
})
export class TakephotoPage {
photo:any = "assets/imgs/photo.png";
imgdata:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private camera:Camera, private restProvider:RestProvider,
              private toastCtrl:ToastController, private loadingCtrl:LoadingController,
              private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {

  }

  takePicture(){
      const options: CameraOptions = {
          quality: 80,
          destinationType: this.camera.DestinationType.DATA_URL,
          sourceType: this.camera.PictureSourceType.CAMERA,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          targetWidth: 2048,
          targetHeight: 1080,
      }

      this.camera.getPicture(options).then((imageData) => {
          this.photo =  'data:image/jpeg;base64,' + imageData;
          this.imgdata = imageData;
      }, (err) => {
          // Handle error
      });


  }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position:'top'
        });
        toast.onDidDismiss(() => {
            this.navCtrl.push(ReviewtechPage,{'ticket_id':this.navParams.get('ticket_id')});
        });
        toast.present();
    }

  next(){

      if(this.imgdata === undefined ){
          let alert = this.alertCtrl.create({
              title: 'Add Photo',
              subTitle: 'Please take photo before moving next',
              buttons: ['Dismiss']
          });
          alert.present();
          return;
      }

      let loading = this.loadingCtrl.create({
          content: 'Please wait...'
      });

      loading.present();
      this.restProvider.savePhoto(this.navParams.get('ticket_id'),this.imgdata).then((result)=>{
          this.presentToast(result['msg']);
          loading.dismiss();
      });


  }

}
