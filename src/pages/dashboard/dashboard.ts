import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,App } from 'ionic-angular';
import { ChatPage } from '../chat/chat';
import { HistoryPage } from '../history/history';
import { ProfilePage } from '../profile/profile';
import { RemoteareaPage } from '../remotearea/remotearea';
import { TrbShootPage } from "../trb-shoot/trb-shoot";
import { RestProvider } from "../../providers/rest/rest";
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import {Geolocation, GeolocationOptions} from "@ionic-native/geolocation";
import { TicketsPage } from "../tickets/tickets";

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  public currentUser:any;
  public chat:boolean = false;
  public file_perm:boolean = false;
  public task:any;

  tab1Root = ChatPage;
  tab2Root = HistoryPage;
  tab3Root = RemoteareaPage;
  tab4Root = ProfilePage;
  tab5Root = TrbShootPage;
  tab6Root = TicketsPage;

  constructor(public navCtrl: NavController,
              public navParams: NavParams, public restProv: RestProvider,
              private push: Push, private alertCtrl:AlertController, private app:App,
              private geolocation:Geolocation) {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));

    if(this.currentUser.permissions.indexOf('chat')!== -1){
      this.chat = true;
      this.restProv.setChatStatus(1);
    }

    if(this.currentUser.permissions.indexOf('trouble_shoot')!== -1){
        this.file_perm = true;
    }

      // to check if we have permission
      this.initPush();

    //load location to the server
      this.geolocation.getCurrentPosition().then((resp) => {
          this.restProv.setLoc(this.currentUser.token,resp.coords.latitude,resp.coords.longitude).then((result)=>{
             console.log(result['msg']);
          });
      }).catch((error) => {
          console.log('Error getting location', error);
      });

      const subscription = this.geolocation.watchPosition()
          .subscribe(position => {
              if(position.coords.longitude != undefined) {
                  this.restProv.setLoc(this.currentUser.token, position.coords.latitude, position.coords.longitude).then((result) => {
                      console.log(result['msg']);
                  });
                  console.log(position.coords.longitude + ' ' + position.coords.latitude);
              }
          });

      //for chat status
      this.task = setInterval(() => {
          this.restProv.setChatStatus(1);
      }, 30000);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }
  initPush(){
      const options: PushOptions = {
          android: {
              senderID: '800934935444',
          },
          ios: {
              alert: 'true',
              badge: true,
              sound: 'false'
          },
          windows: {},
          browser: {
              pushServiceURL: 'http://push.api.phonegap.com/v1/push',
          }
      };

      const pushObject: PushObject = this.push.init(options);

      pushObject.on('notification').subscribe((notification: any) => {
          if(notification.message.indexOf('chat') != -1)
                this.presentChatAlert(notification.message);
          else if(notification.message.indexOf('ticket') != -1)
              this.presentTicketAlert(notification.message);
          else
              this.presentAlert(notification.message);

      });

      pushObject.on('registration').subscribe((registration: any) => {
          this.restProv.setChatToken(registration.registrationId).then((result)=>{
             console.log(result['msg']);
          });
          console.log('Device registered', registration.registrationId);
      });

      pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }

    presentAlert(msg) {
        let alert = this.alertCtrl.create({
            title: 'Message',
            subTitle: msg,
            buttons: ['Dismiss']
        });
        alert.present();
    }
    presentChatAlert(msg) {
        let alert = this.alertCtrl.create({
            title: 'New Chat',
            subTitle: msg,
            buttons: [{
                text: 'OK',
                role: 'confirm',
                handler: () => {
                    //this.navCtrl.push(ChatPage);
                    this.app.getRootNav().getActiveChildNav().select(4);
                }
            }]
        });
        alert.present();
    }

    presentTicketAlert(msg) {
        let alert = this.alertCtrl.create({
            title: 'Ticket',
            subTitle: msg,
            buttons: [{
                text: 'OK',
                role: 'confirm',
                handler: () => {
                    this.app.getRootNav().getActiveChildNav().select(3);
                }
            }]
        });
        alert.present();
    }


}
