import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular'
import { PreviousticketsPage } from '../previoustickets/previoustickets';
import { RestProvider } from "../../providers/rest/rest";
import { TicketupdatePage } from "../ticketupdate/ticketupdate";

/**
 * Generated class for the JobdonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-jobdone',
  templateUrl: 'jobdone.html',
})
export class JobdonePage {

  constructor(public actionSheetCtrl: ActionSheetController,
              public navCtrl: NavController,
              public navParams: NavParams,
              private toastCtrl: ToastController,
              private restProvider: RestProvider,
              ) {
    //do nothing here
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad JobdonePage');
  }

  jobDone(){
      /*this.restProvider.stopTicket(this.navParams.get('ticket_id')).then((result)=>{
          this.presentToast(result['msg']);
      });*/
      this.navCtrl.push(TicketupdatePage,{'ticket_id':this.navParams.get('ticket_id')})
  }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position:'top'
        });
        toast.onDidDismiss(() => {
            this.navCtrl.pop();
        });
        toast.present();
    }


  ticketHistoryfunc(){
    this.navCtrl.push(PreviousticketsPage,{'screen_id':this.navParams.get('screen_id')});
  }

  }

