import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JobdonePage } from './jobdone';

@NgModule({
  declarations: [
    JobdonePage,
  ],
  imports: [
    IonicPageModule.forChild(JobdonePage),
  ],
})
export class JobdonePageModule {}
