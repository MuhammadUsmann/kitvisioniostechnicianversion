import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { RestProvider } from "../../providers/rest/rest";

/**
 * Generated class for the ChatDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat-details',
  templateUrl: 'chat-details.html',
})
export class ChatDetailsPage {
  username:string;
  messages:any;
  message_data:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider:RestProvider, public toastCtrl:ToastController) {
  }

  ionViewWillEnter() {
    this.username = this.navParams.get('username');
     this.loadchat();
  }

  loadchat(){
      this.restProvider.getMessages(this.navParams.get('thread_id')).then(result=>{
         /* Array.prototype.sort.call(result, function (a, b) {
              if (a.chat_id > b.chat_id) {
                  return 1;
              }
              if (a.chat_id < b.chat_id) {
                  return -1;
              }
              return 0;
          });*/
              this.messages = result;
          }, (err) => {
              console.log(err);
          }
      );
  }
    sendmsg(){
        this.restProvider.sendMessage(this.message_data,this.navParams.get('user_id'),this.navParams.get('thread_id'))
            .then((result) => {
                if(result['error'])
                {
                    this.presentToast("Something goes wrong");

                }
                else
                {
                    this.presentToast(result['msg']);
                    this.message_data= "";
                    this.loadchat();
                }

            }, (err) => {
                console.log(err);
            });
    }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position:'top'
        });
        toast.present();
    }

    doRefresh(refresher) {
        this.restProvider.getMessages(this.navParams.get('thread_id')).then(result=>{
           /* Array.prototype.sort.call(result, function (a, b) {
                if (a.chat_id > b.chat_id) {
                    return 1;
                }
                if (a.chat_id < b.chat_id) {
                    return -1;
                }
                return 0;
            });*/
                this.messages = result;
                refresher.complete();
            }, (err) => {
                console.log(err);
                refresher.complete();
            }
        );
    }

}
