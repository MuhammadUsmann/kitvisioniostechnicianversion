import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientsignPage } from './clientsign';

@NgModule({
  declarations: [
    ClientsignPage,
  ],
  imports: [
    IonicPageModule.forChild(ClientsignPage),
  ],
})
export class ClientsignPageModule {}
