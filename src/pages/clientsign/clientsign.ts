import { Component } from '@angular/core';
import { ViewChild } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { SignaturePad } from "angular2-signaturepad/signature-pad";
import { RestProvider } from "../../providers/rest/rest";
import { HistoryPage } from "../history/history";
import { LoadingController} from "ionic-angular";

/**
 * Generated class for the ClientsignPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-clientsign',
  templateUrl: 'clientsign.html',
})
export class ClientsignPage {
    @ViewChild(SignaturePad) public signaturePad : SignaturePad;

    public signaturePadOptions : Object = {
        'minWidth': 2,
        'canvasWidth': 340,
        'canvasHeight': 200
    };
    public signatureImage : string;

  constructor(public toastCtrl: ToastController, public navCtrl: NavController, public navParams: NavParams,
              public loadingCtrl:LoadingController, public restProvider:RestProvider,
              private alertCtrl: AlertController) {
  }


  submit(){
      this.signatureImage = this.signaturePad.toDataURL();

      if(this.signatureImage.length == 0){
          let alert = this.alertCtrl.create({
              title: 'Signuature',
              subTitle: 'Please sign before moving next',
              buttons: ['Dismiss']
          });
          alert.present();
          return;
      }

      let loading = this.loadingCtrl.create({
          content: 'Please wait...'
      });

      loading.present();
      this.restProvider.saveSign(this.navParams.get('ticket_id'),this.signatureImage).then((result)=>{
          loading.dismiss();
            this.presentToast(result['msg']);
      });

  }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position:'top'
        });
        toast.onDidDismiss(() => {
            this.navCtrl.popToRoot();
        });
        toast.present();
    }

    drawClear() {
        this.signaturePad.clear();
    }

}


