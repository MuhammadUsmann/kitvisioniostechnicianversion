import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreviousticketsPage } from './previoustickets';

@NgModule({
  declarations: [
    PreviousticketsPage,
  ],
  imports: [
    IonicPageModule.forChild(PreviousticketsPage),
  ],
})
export class PreviousticketsPageModule {}
