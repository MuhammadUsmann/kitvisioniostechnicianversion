import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from "../../providers/rest/rest";

/**
 * Generated class for the PreviousticketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-previoustickets',
  templateUrl: 'previoustickets.html',
})
export class PreviousticketsPage {
    history:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private restProvider: RestProvider) {
      this.restProvider.getScreenHistory(navParams.get('screen_id')).then((result)=>{
           this.history = result;
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreviousticketsPage');
  }

}
