import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChatDetailsPage } from "../chat-details/chat-details";
import { RestProvider } from "../../providers/rest/rest";

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  chats:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider:RestProvider) {
  }

  ionViewWillEnter() {
    this.restProvider.getAllChats().then((result) => {
            this.chats = result;
    }, (err) => {
        console.log(err);
    });
  }

  chatclick(user_id,username, thread_id){
      this.navCtrl.push(ChatDetailsPage,{'user_id':user_id,'username':username, 'thread_id':thread_id});
  }

    doRefresh(refresher) {
        this.restProvider.getAllChats().then((result) => {
            this.chats = result;
            refresher.complete();
        }, (err) => {
            console.log(err);
            refresher.complete();
        });
    }
}
