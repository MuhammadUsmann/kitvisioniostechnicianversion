import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { TicketupdatePage } from '../ticketupdate/ticketupdate';
import { RestProvider } from '../../providers/rest/rest';
import { TechrequestPage } from "../techrequest/techrequest";
import { TechbookedPage } from "../techbooked/techbooked";
import { JobdonePage } from "../jobdone/jobdone";
import { ReviewtechPage } from "../reviewtech/reviewtech";
import { TakephotoPage } from "../takephoto/takephoto";

/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  userInfo:any;
  history:any;
  currentUserData:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl:ToastController,public restProvider: RestProvider ) {
      this.userInfo = JSON.parse( localStorage.getItem('userInfo'));
      this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
      this.getAllHistory(this.currentUserData.token);
  }

  ionViewWillEnter() {
      this.userInfo = JSON.parse( localStorage.getItem('userInfo'));
      this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
      this.getAllHistory(this.currentUserData.token);
  }

    getAllHistory(token)
    {
        this.restProvider.getHistory(token)
            .then((result) => {
                /*console.log("here i am");
                console.log(result);*/

                if(result['error'])
                {
                    this.presentToast("Something goes wrong");

                }
                else
                {
                    this.history = result;
                }

            }, (err) => {
                console.log(err);
            });
    }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position:'top'
        });
        toast.present();
    }

  ticketUpdate(){
    this.navCtrl.push(TicketupdatePage);
  }

    ticketclick(ticket_id,approved,screen_id){
      console.log(ticket_id);
          if(approved == 0)
              this.navCtrl.push(TechrequestPage,{'ticket_id':ticket_id});
          else if(approved == 1)
              this.navCtrl.push(TechbookedPage,{'ticket_id':ticket_id, 'screen_id':screen_id});
          else if(approved == 2)
              this.navCtrl.push(JobdonePage,{'ticket_id':ticket_id, 'screen_id':screen_id});
          else if(approved == 3)
              this.navCtrl.push(TakephotoPage,{'ticket_id':ticket_id, 'screen_id':screen_id});
    }

    doRefresh(refresher) {
        this.getAllHistory(this.currentUserData.token);
        refresher.complete();
    }

}
