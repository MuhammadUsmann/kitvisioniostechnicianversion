import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { AlertController } from 'ionic-angular';
import {DashboardPage} from "../dashboard/dashboard";

/**
 * Generated class for the ResetpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resetpassword',
  templateUrl: 'resetpassword.html',
})
export class ResetpasswordPage {
credential ={email:''};
  constructor(private alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams,public restProvider: RestProvider,private toastCtrl:ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetpasswordPage');
  }
  resetEmail(){
      this.restProvider.resetpwd(this.credential.email).then((result) => {
        console.log(result);
          let alert = this.alertCtrl.create({
              title: 'DONE',
              message: result['msg'],
              buttons: [
                  {
                      text: 'OK',
                      role: 'OK',
                      handler: () => {
                          console.log('Ok clicked');
                      }
                  }
              ]
          });
          alert.present();

      }, (err) => {
          console.log(err);
      });

  }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 2000
        });
        toast.present();
    }
}
