import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import {TechbookedPage} from "../techbooked/techbooked";
import {TechrequestPage} from "../techrequest/techrequest";
import {JobdonePage} from "../jobdone/jobdone";
import {ReviewtechPage} from "../reviewtech/reviewtech";
import {RestProvider} from "../../providers/rest/rest";
import { Geolocation } from '@ionic-native/geolocation';
import {HistoryPage} from "../history/history";

/**
 * Generated class for the RemoteareaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-remotearea',
  templateUrl: 'remotearea.html',
})
export class RemoteareaPage {
    userInfo:any;
    history:any;
    currentUserData:any;
    latitude:any;
    longitude:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private  restProvider:RestProvider,
              private toastCtrl:ToastController,
              private alertCtrl:AlertController,
              private geolocation: Geolocation) {
      this.userInfo = JSON.parse( localStorage.getItem('userInfo'));
      this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
      this.getAllHistory();
      this.geolocation.getCurrentPosition().then((resp) => {
          this.latitude = resp.coords.latitude;
          this.longitude = resp.coords.longitude;
      }).catch((error) => {
          console.log('Error getting location', error);
      });
  }

  ionViewWillEnter(){
      this.getAllHistory();
  }

    getAllHistory()
    {
        this.restProvider.getRemote()
            .then((result) => {
                /*console.log("here i am");
                console.log(result);*/

                if(result['error'])
                {
                    this.presentToast("Something goes wrong");

                }
                else
                {
                    this.history = result;
                }

            }, (err) => {
                console.log(err);
            });
    }

    doRefresh(refresher) {
        this.getAllHistory();
        refresher.complete();
    }

    reject(ticket_id)
    {
        {
            let alert = this.alertCtrl.create({
                title: 'Confirm',
                message: 'Do you want to Reject Request?',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('cancel clicked');
                        }

                    },
                    {
                        text: 'OK',
                        role: 'OK',
                        handler: () => {
                            this.restProvider.declineTicket(ticket_id).then( (result)=>{
                                this.presentToast(result['msg']);
                            }, (err) => {
                                console.log(err);
                            });
                        }

                    }
                ]
            });
            alert.present();
        }
    }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position: 'top'
        });

        toast.onDidDismiss(() => {
            this.getAllHistory();
        });

        toast.present();
    }

    accept(ticket_id){
        {
            let alert = this.alertCtrl.create({
                title: 'Confirm',
                message: 'Do you want to Accept Request?',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('cancel clicked');
                        }

                    },
                    {
                        text: 'OK',
                        role: 'OK',
                        handler: () => {
                            this.restProvider.acceptTicket(ticket_id,this.latitude,this.longitude).then( (result)=>{
                                this.presentToast(result['msg']);
                            }, (err) => {
                                console.log(err);
                            });
                        }

                    }
                ]
            });
            alert.present();
        }
    }


}
