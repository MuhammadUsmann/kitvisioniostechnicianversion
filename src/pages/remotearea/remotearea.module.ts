import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RemoteareaPage } from './remotearea';

@NgModule({
  declarations: [
    RemoteareaPage,
  ],
  imports: [
    IonicPageModule.forChild(RemoteareaPage),
  ],
})
export class RemoteareaPageModule {}
