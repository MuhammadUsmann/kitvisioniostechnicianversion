import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {TicketupdatePage} from "../ticketupdate/ticketupdate";
import {JobdonePage} from "../jobdone/jobdone";
import {RestProvider} from "../../providers/rest/rest";
import {TechbookedPage} from "../techbooked/techbooked";
import {TakephotoPage} from "../takephoto/takephoto";
import {TechrequestPage} from "../techrequest/techrequest";

/**
 * Generated class for the TicketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tickets',
  templateUrl: 'tickets.html',
})
export class TicketsPage {

    userInfo:any;
    history:any;
    currentUserData:any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl:ToastController,public restProvider: RestProvider ) {
        this.userInfo = JSON.parse( localStorage.getItem('userInfo'));
        this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.getTickets(this.currentUserData.token);
    }

    ionViewWillEnter() {
        this.userInfo = JSON.parse( localStorage.getItem('userInfo'));
        this.currentUserData = JSON.parse(localStorage.getItem('currentUser'));
        this.getTickets(this.currentUserData.token);
    }

    getTickets(token)
    {
        this.restProvider.getTickets(token)
            .then((result) => {
                /*console.log("here i am");
                console.log(result);*/

                if(result['error'])
                {
                    this.presentToast("Something goes wrong");

                }
                else
                {
                    this.history = result;
                }

            }, (err) => {
                console.log(err);
            });
    }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position:'top'
        });
        toast.present();
    }

    ticketUpdate(){
        this.navCtrl.push(TicketupdatePage);
    }

    ticketclick(ticket_id,approved,screen_id){
        console.log(ticket_id);
        if(approved == 0)
            this.navCtrl.push(TechrequestPage,{'ticket_id':ticket_id});
        else if(approved == 1)
            this.navCtrl.push(TechbookedPage,{'ticket_id':ticket_id, 'screen_id':screen_id});
        else if(approved == 2)
            this.navCtrl.push(JobdonePage,{'ticket_id':ticket_id, 'screen_id':screen_id});
        else if(approved == 3)
            this.navCtrl.push(TakephotoPage,{'ticket_id':ticket_id, 'screen_id':screen_id});
    }

    doRefresh(refresher) {
        this.getTickets(this.currentUserData.token);
        refresher.complete();
    }

}