import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { TakephotoPage } from "../takephoto/takephoto";
import { RestProvider } from "../../providers/rest/rest";
import { AlertController } from "ionic-angular";

/**
 * Generated class for the TicketupdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ticketupdate',
  templateUrl: 'ticketupdate.html',
})
export class TicketupdatePage {
  status:any;
  selected_status:any;
  message:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private toastCtrl:ToastController, private restProvider: RestProvider,
              private alertCtrl: AlertController) {
    this.restProvider.getAllStatuses().then((result)=>{
        this.status = result;
    });
  }

  next(){
      if(this.selected_status == '' || this.message == ''){
          let alert = this.alertCtrl.create({
              title: 'Enter Dtaa',
              subTitle: 'Please select status and enter message before continuing',
              buttons: ['Dismiss']
          });
          alert.present();
          return;
      }

      this.restProvider.stopTicket(this.navParams.get('ticket_id'),this.message,this.selected_status).then((result)=>{
          this.presentToast(result['msg']);
      });

  }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 5000,
            position:'top'
        });
        toast.onDidDismiss(() => {
            this.navCtrl.push(TakephotoPage,{'ticket_id':this.navParams.get('ticket_id')});
        });
        toast.present();
    }
}
