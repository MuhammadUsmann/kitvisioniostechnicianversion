import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TicketupdatePage } from './ticketupdate';

@NgModule({
  declarations: [
    TicketupdatePage,
  ],
  imports: [
    IonicPageModule.forChild(TicketupdatePage),
  ],
})
export class TicketupdatePageModule {}
