import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JobreviewPage } from './jobreview';

@NgModule({
  declarations: [
    JobreviewPage,
  ],
  imports: [
    IonicPageModule.forChild(JobreviewPage),
  ],
})
export class JobreviewPageModule {}
