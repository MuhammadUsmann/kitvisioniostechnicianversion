import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { Geolocation} from "@ionic-native/geolocation";
import { GoogleMaps } from "@ionic-native/google-maps";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { SignaturePadModule} from "angular2-signaturepad";
import { Network } from '@ionic-native/network';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { DashboardPage } from '../pages/dashboard/dashboard';
import { ChatPage } from '../pages/chat/chat';
import { ChatDetailsPage } from "../pages/chat-details/chat-details";
import { HistoryPage } from '../pages/history/history';
import { ProfilePage } from '../pages/profile/profile';
import { RemoteareaPage } from '../pages/remotearea/remotearea';
import { TicketupdatePage } from '../pages/ticketupdate/ticketupdate';
import { TakephotoPage } from '../pages/takephoto/takephoto';
import { ReviewtechPage } from '../pages/reviewtech/reviewtech';
import { ClientsignPage } from '../pages/clientsign/clientsign';
import { TechrequestPage } from '../pages/techrequest/techrequest';
import { TechbookedPage } from '../pages/techbooked/techbooked';
import { ResetpasswordPage } from '../pages/resetpassword/resetpassword';
import { StartrepairPage } from '../pages/startrepair/startrepair';
import { JobdonePage } from '../pages/jobdone/jobdone';
import { JobreviewPage } from '../pages/jobreview/jobreview';
import { PreviousticketsPage } from '../pages/previoustickets/previoustickets';
import { TrbShootPage } from "../pages/trb-shoot/trb-shoot";
import { HttpClientModule } from '@angular/common/http';
import { RestProvider } from '../providers/rest/rest';
import {SignaturePad} from "angular2-signaturepad/signature-pad";
import {TicketsPage} from "../pages/tickets/tickets";




@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DashboardPage,
    ChatPage,
    HistoryPage,
    ProfilePage,
    RemoteareaPage,
    TicketupdatePage,
    TakephotoPage,
    ReviewtechPage,
    ClientsignPage,
    TechrequestPage,
    TechbookedPage,
    ResetpasswordPage,
    StartrepairPage,
    JobdonePage,
    JobreviewPage,
    PreviousticketsPage,
    ChatDetailsPage,
    TrbShootPage,
    TicketsPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
      SignaturePadModule,
    HttpClientModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DashboardPage,
    ChatPage,
    HistoryPage,
    ProfilePage,
    RemoteareaPage,
    TicketupdatePage,
    TakephotoPage,
    ReviewtechPage,
    ClientsignPage,
    TechrequestPage,
    TechbookedPage,
    ResetpasswordPage,
    StartrepairPage,
    JobdonePage,
    JobreviewPage,
    PreviousticketsPage,
    ChatDetailsPage,
    TrbShootPage,
    TicketsPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
      RestProvider,
      Push,
      Geolocation,
      GoogleMaps,
      Camera,
      SignaturePadModule,
      SignaturePad,
      Network,
  ]
})
export class AppModule {}
